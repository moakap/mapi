// Package db provides database operations like migrations, sync
// table schemes also database operation hooks base on the gorm.
package db

import (
	"context"
	"errors"
	"fmt"
	"regexp"

	"github.com/jinzhu/gorm"
)

// keyMatcher is for extracting 'id' out of eg. 'users (id)', to
// convert the definition of the constraing column to the format used
// in the constraint name.
var keyMatcher = regexp.MustCompile(" ?\\(([^)]+)\\)?")

// EnsureForeignKey will attempt to add the given foreign-key
// constraint to the database, and swallow any error that is triggered
// if the constraint already exists. The intent is to be an idempotent
// function that is safe to be called multiple times.
func EnsureForeignKey(db *gorm.DB, model interface{}, sourceTable string, column string, table string, onDelete string, onUpdate string) error {
	scope := db.NewScope(model) // Mainly for logging
	err := db.Model(model).AddForeignKey(column, table, onDelete, onUpdate).Error
	if err != nil {
		tableFragment := keyMatcher.ReplaceAllString(table, "_${1}")
		expectedMsg := fmt.Sprintf("pq: constraint \"%s_%s_%s_foreign\" for relation \"%s\" already exists", sourceTable, column, tableFragment, sourceTable)

		if err.Error() == expectedMsg {
			scope.Log(fmt.Sprintf("Foreign-key constraint appears to already exist: %s", err.Error()))
		} else {
			return err
		}
	}
	return nil
}

// EnsureConstraint will attempt to add the given table constraint
// to the database, and swallow any error that is triggered if the
// constraint already exists. The intent is to be an idempotent
// function that is safe to be called multiple times, similar to
// EnsureForeignKey.
func EnsureConstraint(db *gorm.DB, model interface{}, name string, constraint string) error {
	scope := db.NewScope(model)
	query := `ALTER TABLE %s ADD CONSTRAINT %s %s;`

	err := db.Exec(fmt.Sprintf(query, scope.QuotedTableName(), scope.Quote(name), constraint)).Error
	if err != nil {
		expectedMsg := fmt.Sprintf("pq: constraint \"%s\" for relation \"%s\" already exists", name, scope.TableName())

		if err.Error() == expectedMsg {
			scope.Log(fmt.Sprintf("Constraint appears to already exist: %s", err.Error()))
		} else {
			return err
		}
	}
	return nil
}

// EnsureIndex will attempt to create the index for the given
// table, and swallow any error that is triggered if the index
// already exists. The intent is to be an idempotent function
// that is safe to be called multiple times, similar to
// EnsureForeignKey.
//
// Eventually it would be better to switch to Postgres 9.4+ and
// use `CREATE INDEX IF NOT EXISTS ...`, then this function can
// be removed.
func EnsureIndex(db *gorm.DB, model interface{}, name string, constraint string, unique bool) error {
	scope := db.NewScope(model)
	query := `CREATE INDEX %s ON %s;`

	if unique {
		query = `CREATE UNIQUE INDEX %s ON %s;`
	}

	err := db.Exec(fmt.Sprintf(query, scope.Quote(name), constraint)).Error
	if err != nil {

		expectedMsg := fmt.Sprintf("pq: relation \"%s\" already exists", name)

		if err.Error() == expectedMsg {
			scope.Log(fmt.Sprintf("Index appears to already exist: %s", err.Error()))
		} else {
			return err
		}
	}
	return nil
}

// ExecInTransaction will execute `f` inside a transaction, ensuring that
// the transaction is `Commit`ed or `Rollback`ed [sic] even if `f` or
// any code called by `f` panics. Transaction will commit if `f`
// returns nil, otherwise will rollback.
func ExecInTransaction(db *gorm.DB, f func(*gorm.DB) error) error {
	commit := false
	tx := db.Begin()
	defer func() {
		if commit {
			tx.Commit()
		} else {
			tx.Rollback()
		}
	}()

	err := f(tx)
	commit = err == nil
	return err
}

// internal key to store context to gorm.BD
const ctxKey = "_context"

// WithContext creates a new `*gorm.DB` with the given context
// installed. This will make the context available in situations where
// you only have access to a `*gorm.DB`, and not a `*http.Request`.
func WithContext(ctx context.Context, db *gorm.DB) *gorm.DB {
	return db.Set(ctxKey, ctx)
}

// GetContext extracts a context from a `*gorm.DB` if present.
func GetContext(db *gorm.DB) (context.Context, bool) {
	if v, ok := db.Get(ctxKey); ok {
		ctx, ok := v.(context.Context)
		return ctx, ok
	}
	return nil, false
}

// ForceGetContext extracts a context from a `*gorm.DB` if present, and
// `panic`s if there is no context in the `*gorm.DB`.
func ForceGetContext(db *gorm.DB) context.Context {
	if ctx, ok := GetContext(db); ok {
		return ctx
	}

	panic(errors.New("no context set in gorm"))
}
