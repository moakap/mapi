package db

import (
	"sync"

	"errors"

	"github.com/jinzhu/gorm"
)

// IdDB returns the given *gorm.DB. It is used as a anonymity scope
// function.
func IdDB(db *gorm.DB) *gorm.DB {
	return db
}

// QueryInParallel calls the given query function in another goroutine,
// it is assuming the given db carries the request context.
func QueryInParallel(wg *sync.WaitGroup, db *gorm.DB, queryFunc func(db *gorm.DB) error) {

	wg.Add(1)
	go func() {
		defer wg.Done()
		if err := queryFunc(db.New()); err != nil {
			errors.New("error calling query function in dbutils.QueryInParallel:" + err.Error())
			return
		}
	}()
}
