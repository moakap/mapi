package db

import (
	"github.com/gin-gonic/gin"
	"github.com/jinzhu/gorm"
	"github.com/theplant/appkit/log"
)

// Config is for configuration that you can embed in your app config.
type Config struct {
	Dialect string `default:"mysql"`
	Params  string `required:"true"`
}

// New creates a DB object.
func New(l log.Logger, config Config) (*gorm.DB, error) {
	var err error
	var db *gorm.DB

	l = l.With("context", "db/db.New")
	l.Debug().Log("msg", "opening database connection")

	db, err = gorm.Open(config.Dialect, config.Params)
	if err != nil {
		l.Error().Log(
			"msg", "Open connection (gorm.Open) to DB failed. ",
			"error", err.Error(),
		)
		return db, err
	}

	// enable the log mode
	db.LogMode(true)

	l.Debug().Log("msg", "database good to go")
	return db, nil
}

// Customer middleware to bind DB to context
// https://github.com/gin-gonic/gin#custom-middleware
func DBMiddleware(db *gorm.DB) gin.HandlerFunc {
	return func(c *gin.Context) {
		c = ContextWithGorm(c, db)

		c.Next()
	}
}

// Transaction will execute `f` inside a transaction, ensuring that
// the transaction is `Commit`ed or `Rollback`ed [sic] even if `f` or
// any code called by `f` panics. Transaction will commit if `f`
// returns nil, otherwise will rollback.
func Transaction(db *gorm.DB, f func(*gorm.DB) error) error {
	commit := false
	tx := db.Begin()
	defer func() {
		if commit {
			tx.Commit()
		} else {
			tx.Rollback()
		}
	}()

	err := f(tx)
	commit = err == nil
	return err
}
