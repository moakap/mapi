package db

import (
	"github.com/gin-gonic/gin"
	"github.com/jinzhu/gorm"
)

// internal key to store context to gorm.BD
const ctxKeyGorm = "_gorm"

// WithGorm binds gorm DB to context so that db can be used during the whole context.
func ContextWithGorm(c *gin.Context, db *gorm.DB) *gin.Context {
	// clone another instance so that it will not break existing DB.
	newDB := db.New()

	c.Set(ctxKeyGorm, newDB)
	return c
}

// Get gorm DB from context.
func GetGorm(c *gin.Context) (*gorm.DB, bool) {
	db, ok := c.Get(ctxKeyGorm)
	return db.(*gorm.DB), ok
}

// MustGetGorm is another variant to get gorm DB from context, but panic when no gorm.
func MustGetGorm(c *gin.Context) *gorm.DB {
	db, ok := GetGorm(c)

	if !ok {
		panic("can not find gorm in context")
	}

	return db
}
