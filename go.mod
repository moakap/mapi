module gitlab.com/moakap/mapi

go 1.14

require (
	github.com/Pallinder/go-randomdata v1.2.0
	github.com/RangelReale/osin v1.0.1
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/gin-gonic/gin v1.6.3
	github.com/jinzhu/configor v1.2.0
	github.com/jinzhu/gorm v1.9.16
	github.com/pborman/uuid v1.2.1
	github.com/pkg/errors v0.9.1
	github.com/qiniu/api.v7/v7 v7.6.0
	github.com/theplant/appkit v0.0.0-20200710051107-b5c999e99992
	golang.org/x/crypto v0.0.0-20201002170205-7f63de1d35b0
	gopkg.in/go-playground/validator.v8 v8.18.2
)
