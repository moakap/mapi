// Package config provides the app's configuration.
package appconfig

import (
	"fmt"

	"github.com/jinzhu/configor"
	"github.com/theplant/appkit/log"
	"github.com/theplant/appkit/server"
	"gitlab.com/moakap/mapi/common/accesscontrol"
	"gitlab.com/moakap/mapi/db"
	"gitlab.com/moakap/mapi/users/jwtgenerator"
	"gitlab.com/moakap/mapi/utils/qiniup"
	"gitlab.com/moakap/mapi/utils/yunsms"
)

// AppConfig defines the app's configuration
type AppConfig struct {
	// AppRoot indicates the application's source code directory location.
	AppRoot string `required:"true"`

	// UrlRoot indicates the base URL of API endpoints.
	UrlRoot string `required:"true"`

	HTTP server.Config
	DB   db.Config
	JWT  jwtgenerator.Config

	// AccessControl
	AccessControl accesscontrol.Config

	// Qiniu config
	Qiniu qiniup.Config

	// YunSMS
	SMS yunsms.YunSmsConfig

	// jisuapi
	JisuAppKey string

	// Centrifugo Secret
	CentrifugoSecret string
}

// APIRoot is a temporary wrapper around UrlRoot to enable access via
// an `*AppConfig`
func (c *AppConfig) APIRoot() string {
	return fmt.Sprintf("%s%s", c.UrlRoot, "/api")
}

// New loads the app's configuration from the environment, and
// configures necessary subsystems.
func New(logger log.Logger) (config *AppConfig, err error) {
	config = &AppConfig{}

	err = configor.Load(config)

	if err != nil {
		logger.Crit().Log(
			"during", "config.Start",
			"err", err,
			"msg", fmt.Sprintf("error loading config: %v", err),
		)
	}

	return
}
