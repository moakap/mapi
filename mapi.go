package mapi

import (
	"fmt"
	"net/http"

	"gitlab.com/moakap/mapi/appconfig"

	"github.com/gin-gonic/gin"
	"github.com/jinzhu/gorm"
	"github.com/theplant/appkit/log"
	"gitlab.com/moakap/mapi/users"
	um "gitlab.com/moakap/mapi/users/models"
	"gitlab.com/moakap/mapi/users/oauthprovider"
	"gitlab.com/moakap/mapi/utils"

	appdb "gitlab.com/moakap/mapi/db"
)

/** New returns a gin.Engine with basic mapi routes mounted already.
routes mounted includes:
**** oauth 认证相关
POST /oauth/token
POST /oauth/verification_code
POST /oauth/set_password

**** user相关
GET /profile 用户查看自己信息
GET /users/:id 后台获取用户信息
PATCH /users/:id 修改用户信息

**** ping 测试
GET /ping Testing purpose

** wechat 微信相关
GET /wechat/session_key get wechat session key
POST /wechat/register
*/
func NewMapiEngine(logger log.Logger, c *appconfig.AppConfig, db *gorm.DB) (engine *gin.Engine, openApi *gin.RouterGroup, authApi *gin.RouterGroup) {
	engine = gin.New()

	// migrate DB
	um.AutoMigrate(db)

	// User DBMiddleware to bind DB to context.
	engine.Use(appdb.DBMiddleware(db))

	// Mount oauth enpoint to grant access token / login.
	oauthGroup := engine.Group("/oauth")
	op := oauthprovider.MountTo(oauthGroup, c)

	// Use Authenticator middleware to get access data from token header.
	// Note: it will NOT abort the request if no/invalid access token.
	openApi = engine.Group("/api", op.Authenticator)

	// utls API
	utilsGroup := openApi.Group("/utils")
	utils.UtilsRegister(c, utilsGroup)

	// ******** Auth API that need access token.
	authAPIGroup := openApi.Group("/", users.MustAuthUser)

	// mount USERS
	users.UsersRegister(c, authAPIGroup) //user

	totalGroup := engine.Group("")
	{
		//微信小程序获取session_key（小程序使用）
		totalGroup.GET("/wechat/session_key", users.GetWechatSessionKey)
		totalGroup.GET("/wechat/access_token", users.RouteGetWxAccessToken)

		//微信注册
		totalGroup.POST("/wechat", users.RegisterByWxProfile)
	}

	// For testing purpose.
	fmt.Println("==== enter TestAPI....")
	openApi.GET("/ping", func(c *gin.Context) {
		c.JSON(http.StatusOK, gin.H{
			"message": "pong",
		})
	})

	return engine, openApi, authAPIGroup

}

// func main(){
// 	// How to use mapi
// 	var appConfig *appconfig.AppConfig
// 	var gormDB *gorm.DB
// 	var err error

// 	// Create logger for global usage
// 	logger := log.Default()
// 	l := logger.With("origin", "config")
// 	appConfig, err = appconfig.New(l)
// 	if err != nil {
// 		panic(err)
// 	}

// 	// Open connection to DB
// 	dbLogger := logger.With("origin", "gorm")
// 	gormDB, err = appdb.New(dbLogger, appConfig.DB)
// 	defer gormDB.Close()
// 	if err != nil {
// 		panic(err)
// 	}

// 	// Migrate the DB tables.
// 	// Migrate(gormDB)

// 	// Http routes
// 	httpLogger := logger.With("origin", "http")
// 	handler := routes.New(httpLogger, appConfig, gormDB)

// 	//
// 	engine, api, authAPIGroup := NewMapiEngine()

// 	// MOUNT more application routes here...
// 	// // mount BOOKS
// 	// br.MountBooks(authAPIGroup)

// 	// // mount FRIENDS
// 	// fr.MountFriends(authAPIGroup)

// 	mux := http.NewServeMux()
// 	mux.Handle("/", engine)

// 	middlewares := server.Compose(
// 		accesscontrol.AllowCrossDomainRequest(c.AccessControl),
// 	)

// 	handler := middlewares(mux)

// 	server.ListenAndServe(appConfig.HTTP, httpLogger, handler)
// 	// return middlewares(mux)
// }
