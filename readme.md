# NOEBook

# How it works
```
.
├── appconfig
│   ├── appconfig.go    //整个App相关配置的读取
│   ├── envrc.xxx       //配置样例
├── db
│   ├── context.go      //gin上下文中绑定、获取数据库链接
│   ├── db.go           //数据库连接接口
│   └── db_utils.go     //数据库相关的工具函数，例如：数据库验证函数等。
├── common
│   ├── xxx.go          //其它通用函数
│   └── model.go        //数据库通用Model
├── users               //用户模块
├── ...                 //xx模块
└── users               //用户模块

```

# Getting started

## 安装Golang
https://golang.org/doc/install

同时确保 $GOROOT, $GOPATH等环境变量设置生效。 

## 按照Govendor
项目使用govendor来管理相关依赖包。

https://github.com/kardianos/govendor

```
go get -u github.com/kardianos/govendor
```

## 本地启动
```
➜  cd &PATH_TO_SRC/backend
➜  govendor sync
➜  go build
➜  source appconfig/envrc.local
➜  ./backend
```

## 部署到开发/测试环境
TBD

## Testing
```
➜  go test -v ./... -cover
```
