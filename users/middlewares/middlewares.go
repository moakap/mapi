package middlewares

// Authenticator is middleware to process OAuth bearer token into
// context if it is set and valid. Note that it will not abort the
// request if the token is absent, as this is the responsibility of
// each individual resource/handler.
// func (p *Provider) Authenticator(context *gin.Context) {
// 	logger := appkitlog.ForceContext(context.Request.Context())

// 	accessData, err := p.auth(context)
// 	if err != nil {

// 		logger.Error().Log(
// 			"msg", fmt.Sprintf("auth error: %s", err),
// 			"during", "oauthprovider.Provider.auth",
// 			"err", err,
// 		)
// 		context.Error(err)
// 		// Needs to set authentication header
// 		context.AbortWithStatus(http.StatusUnauthorized)
// 	} else {
// 		context.Set(accessDataKey, accessData)
// 		context.Next()
// 	}
// }

// // EnsureUserExists processor ensures the user (provided by the
// // user processor) exists and pass the user to the handler function.
// //
// // will abort with 401 and not call wrapped function if there is
// // no user. panics if the user provider do not provide a user.
// var EnsureUserExists = resources.CurryUserProcessor(func(accepter resources.UserHandler, ctx *gin.Context, user resources.User) {
// 	// The cast is required here due to interface with the empty pointer isn't equal to nil.
// 	// For reference: https://play.golang.org/p/FzAcgKgxcY
// 	if user.(*accounts.User) == nil {
// 		ctx.AbortWithError(http.StatusUnauthorized, oauthprovider.ErrAccessDenied)
// 	} else {
// 		accepter(ctx, user)
// 	}
// })

// // EnsureAdminUserOrOwner processor ensures the user (provided by the user processor)
// // is an `Admin` user or the owner of the model and pass the user and model to the
// // handler function.
// //
// // will abort with 401 and not call wrapped function if the user doesn't own te model
// // and not an `Admin` user. Panics if the user provider do not provide a user.
// var EnsureAdminUserOrOwner = resources.CurryUserModelProcessor(func(accepter resources.UserModelHandler, ctx *gin.Context, user resources.User, model resources.DBModel) {
// 	if user.GetID() == model.OwnerID() || isAdminUser(user) {
// 		accepter(ctx, user, model)
// 	} else {
// 		ctx.AbortWithError(http.StatusUnauthorized, oauthprovider.ErrAccessDenied)
// 	}
// })
