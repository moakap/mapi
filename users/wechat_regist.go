package users

import (
	"net/http"

	randomdata "github.com/Pallinder/go-randomdata"
	"github.com/gin-gonic/gin"

	"gitlab.com/moakap/mapi/db"
	"gitlab.com/moakap/mapi/users/models"

	appkitlog "github.com/theplant/appkit/log"
)

//获取用户微信信息
type WxProfileParam struct {
	Mobile           string
	VerificationCode string

	NickName   string
	Sex        float64
	City       string
	Country    string
	Province   string
	Language   string
	HeadImgUrl string

	//mandatory fields
	OpenID  string `binding:"required"`
	UnionID string `binding:"required"`

	//
	ClientUserID uint
}

//The method of WeChat login connetc to mobile
func RegisterByWxProfile(ctx *gin.Context) {
	logger := appkitlog.ForceContext(ctx.Request.Context())
	logger = logger.With("func", "GetWechatSessionKey")

	wxProfileParam := WxProfileParam{}
	if err := ctx.BindJSON(&wxProfileParam); err != nil {
		logger.Error().Log("msg", "bind JSON failed",
			"error", err.Error(),
		)
		ctx.JSON(http.StatusUnprocessableEntity, gin.H{"error": err.Error()})
		return
	}

	wxProfile := models.WXProfile{
		OpenID:     wxProfileParam.OpenID,
		NickName:   wxProfileParam.NickName,
		Sex:        float64(wxProfileParam.Sex),
		City:       wxProfileParam.City,
		Province:   wxProfileParam.Province,
		Country:    wxProfileParam.Country,
		HeadImgUrl: wxProfileParam.HeadImgUrl,
		UnionID:    wxProfileParam.UnionID,
	}

	//事物
	newDB := db.MustGetGorm(ctx)
	tx := newDB.Begin()

	//创建wxProfile
	if err := tx.Create(&wxProfile).Error; err != nil {
		tx.Rollback()

		logger.Error().Log("msg", "save wxprofile to DB failed",
			"error", err.Error(),
		)
		ctx.JSON(http.StatusUnprocessableEntity, gin.H{"error": err.Error()})
		return
	}

	//判断用户是否已经注册
	user := models.User{
		Username: wxProfileParam.UnionID,
		Role:     models.ROLE_CUSTOMER,
		UnionID:  wxProfileParam.UnionID,
	}
	if err := user.SetPassword(randomdata.SillyName()); err != nil {
		tx.Rollback()

		logger.Error().Log("msg", "set random password for new user failed",
			"error", err.Error(),
		)
		ctx.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}

	if err := tx.Create(&user).Error; err != nil {
		tx.Rollback()

		logger.Error().Log("msg", "create new user with wxprofile failed",
			"error", err.Error(),
			"unionid", user.UnionID,
		)
		ctx.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}

	if err := tx.Commit().Error; err != nil {
		tx.Rollback()

		logger.Error().Log("msg", "commit changes to create new user failed",
			"error", err.Error(),
			"unionid", user.UnionID,
		)
		ctx.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}

	ctx.JSON(http.StatusOK, gin.H{"data": user})
}
