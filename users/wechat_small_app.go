package users

import (
	"encoding/json"
	"errors"
	"io/ioutil"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
	appkitlog "github.com/theplant/appkit/log"
)

const (
	WX_APP_ID     = "wxa051daca413fbe51"
	WX_APP_SECRET = "7a96e5912b69bdb1db33e0e8771701e1"

	// WX_APP_ID     = "wx3742279ee0f09391"
	// WX_APP_SECRET = "a8eb99a9fae5191333cad3e27eaa7ef5"
)

type SmallReturnUnionID struct {
	Openid     string //用户唯一标识
	SessionKey string
	Unionid    string //用户在开放平台的唯一标识符
}

func GetWechatSessionKey(ctx *gin.Context) {
	logger := appkitlog.ForceContext(ctx.Request.Context())
	logger = logger.With("func", "GetWechatSessionKey")

	code := ctx.Query("code")
	if code == "" {
		logger.Error().Log("msg", "get query parameter failed",
			"name", "code",
		)
		ctx.JSON(http.StatusUnprocessableEntity, errors.New("code传参错误"))
		return
	}

	//获取access_token
	getAcc, err := getAccessTokenByCode(code, logger)
	if err != nil {
		logger.Error().Log("msg", "get session key via code failed",
			"code", code,
			"error", err.Error(),
		)
		ctx.JSON(http.StatusInternalServerError, err)
		return
	}

	ctx.JSON(http.StatusOK, gin.H{"data": getAcc})
}

//通过code获取access_token
func getAccessTokenByCode(code string, logger appkitlog.Logger) (wcrequest SmallReturnUnionID, err error) {
	logger = logger.With("func", "getAccessTokenByCode")
	//
	small_url := "https://api.weixin.qq.com/sns/jscode2session?appid=" +
		WX_APP_ID + "&secret=" +
		WX_APP_SECRET + "&js_code=" +
		code + "&grant_type=authorization_code"

	//get request
	response, err := http.Get(small_url)
	if err != nil {
		logger.Error().Log("msg", "failed to send request",
			"url", small_url,
			"error", err.Error(),
		)
		return wcrequest, err
	}
	defer response.Body.Close()

	//response err
	if response.StatusCode != http.StatusOK {
		errcode := strconv.Itoa(response.StatusCode)
		logger.Error().Log("msg", "get session key by code failed",
			"url", small_url,
			"status_code", response.StatusCode,
		)
		return wcrequest, errors.New("用户请求失败,返回状态码:" + errcode)
	}

	body, err := ioutil.ReadAll(response.Body)
	if err != nil {
		return wcrequest, err
	}

	//var map
	mvalue := make(map[string]interface{})
	err = json.Unmarshal(body, &mvalue)
	if err != nil {
		return wcrequest, err
	}

	//if Exist errcode,return
	if _, ok := mvalue["errcode"]; ok { //返回存在错误
		return wcrequest, errors.New(mvalue["errmsg"].(string))
	}

	if _, ok := mvalue["openid"]; ok { //返回 openid
		wcrequest.Openid = mvalue["openid"].(string)
	}

	if _, ok := mvalue["session_key"]; ok { //返回 session_key
		wcrequest.SessionKey = mvalue["session_key"].(string)
	}

	if _, ok := mvalue["unionid"]; ok { //返回 unionid
		wcrequest.Unionid = mvalue["unionid"].(string)
	}

	return wcrequest, nil
}

type WXBackendAccessToken struct {
	AccessToken string
	ExpiresIn   float64
	ErrorCode   float64
	ErrorMsg    string
}

func RouteGetWxAccessToken(ctx *gin.Context) {
	logger := appkitlog.ForceContext(ctx.Request.Context())
	logger = logger.With("func", "routeGetWxAccessToken")

	appId := ctx.Query("appid")
	appSecret := ctx.Query("appsecret")

	accessToken, err := GetWxAccessToken(appId, appSecret)
	if err != nil {
		logger.Error().Log("msg", "get access token request failed",
			"appid", appId,
			"appsecret", appSecret,
		)
		ctx.JSON(http.StatusUnprocessableEntity, errors.New("请求失败"))
		return
	}

	ctx.JSON(http.StatusOK, gin.H{"data": accessToken})
}

func GetWXBackendAccessToken() (wxAccessCode WXBackendAccessToken, err error) {
	return GetWxAccessToken(WX_APP_ID, WX_APP_SECRET)
}

func GetWxAccessToken(appid string, appsecret string) (wxAccessCode WXBackendAccessToken, err error) {
	// https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=APPID&secret=APPSECRET
	small_url := "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=" +
		appid + "&secret=" + appsecret

	//get request
	response, err := http.Get(small_url)
	if err != nil {
		return wxAccessCode, err
	}
	defer response.Body.Close()

	//response err
	if response.StatusCode != http.StatusOK {
		errcode := strconv.Itoa(response.StatusCode)
		return wxAccessCode, errors.New("用户请求失败,返回状态码:" + errcode)
	}

	body, err := ioutil.ReadAll(response.Body)
	if err != nil {
		return wxAccessCode, err
	}

	//var map
	mvalue := make(map[string]interface{})
	err = json.Unmarshal(body, &mvalue)
	if err != nil {
		return wxAccessCode, err
	}

	//if Exist errcode,return
	if _, ok := mvalue["errcode"]; ok { //返回存在错误
		wxAccessCode.ErrorCode = mvalue["errcode"].(float64)

		if wxAccessCode.ErrorCode == -1 {
			return wxAccessCode, errors.New("系统繁忙，此时请开发者稍候再试")
		}
	}

	if _, ok := mvalue["access_token"]; ok { //返回 openid
		wxAccessCode.AccessToken = mvalue["access_token"].(string)
	}

	if _, ok := mvalue["expires_in"]; ok { //返回 session_key
		wxAccessCode.ExpiresIn = mvalue["expires_in"].(float64)
	}

	if _, ok := mvalue["errmsg"]; ok { //返回 unionid
		wxAccessCode.ErrorMsg = mvalue["errmsg"].(string)
	}

	return wxAccessCode, nil
}
