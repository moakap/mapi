package users

import (
	"errors"
	"fmt"
	"net/http"
	"strconv"

	"github.com/jinzhu/gorm"

	"gitlab.com/moakap/mapi/appconfig"
	"gitlab.com/moakap/mapi/common"
	"gitlab.com/moakap/mapi/db"
	"gitlab.com/moakap/mapi/users/models"
	"gitlab.com/moakap/mapi/users/oauthprovider"
	"gitlab.com/moakap/mapi/users/serializers"
	"gitlab.com/moakap/mapi/utils/qiniup"

	"github.com/gin-gonic/gin"

	appkitlog "github.com/theplant/appkit/log"
	uc "gitlab.com/moakap/mapi/users/controllers"
)

func MustAuthUser(ctx *gin.Context) {
	oc := oauthprovider.NewOsinContext(ctx)
	_, err := oc.UserData()
	if err != nil {
		ctx.AbortWithError(http.StatusUnauthorized, oauthprovider.ErrAccessDenied)
	} else {
		ctx.Next()
	}

}

func MustAuthAdminUser(ctx *gin.Context) {
	oc := oauthprovider.NewOsinContext(ctx)
	user, err := oc.UserData()
	if err != nil || user.Role != "admin" {
		ctx.AbortWithError(http.StatusUnauthorized, oauthprovider.ErrAccessDenied)
	} else {
		ctx.Next()
	}

}

func UsersRegister(c *appconfig.AppConfig, router *gin.RouterGroup) {
	fmt.Println("==== enter UsersRegister....")
	//用户查看自己信息
	router.GET("/profile", routeGetSelfProfileHandler)

	//后台获取用户信息
	router.GET("/users/:id", routeGetOneUserProfileHandler)

	//修改用户信息
	router.PUT("/users/:id", routePatchOneUserHandler)

	// 修改、绑定微信
	router.POST("/users/:id/wx_profile", routePostWXProfileHandler)

	// //根据用户ID获取收货地址
	// router.GET("/users/:id/addresses", GetAddresses)

	// //根据地址ID获取收货地址详情
	// router.GET("/addresses/:id", GetAddress)

	// //新增收货地址
	// router.POST("/addresses", PostAddress)

	// //修改收货地址
	// router.PUT("/addresses/:id", PatchAddress)

	// //删除收货地址
	// router.DELETE("/addresses/:id", DeleteAddress)

	// *********** Start of Group API
	// 获取用户的group
	router.GET("/my_groups", routeMyGroupsHandler)

	// 获取所有groups
	router.GET("/groups", routeGetGroupsHandler)

	// 获取group详情
	router.GET("/groups/:id", routeGetOneGroupHandler)

	// 新增group
	router.POST("/groups", routeNewOrUpdateGroupHandler)

	// 修改
	router.PUT("/group/:id", routeUpdateOneGroupHandler)

	// 关联用户
	router.POST("/attach_to_group/:group_id", routeAttachToGroupHandler)

	// *********** end of Group API

	//获取七牛云上传token
	router.GET("/qn_uptoken", getQiniuUploadTokenHandler(c))
}

func routeGetSelfProfileHandler(c *gin.Context) {
	logger := appkitlog.ForceContext(c.Request.Context())
	logger = logger.With("func", "routeGetSelfProfileHandler")

	requester, err := oauthprovider.GetRequesterFromContext(c)
	if err != nil || requester == nil {
		logger.Error().Log("msg", "unauthorized opeation request, return 401")
		c.JSON(http.StatusUnauthorized, gin.H{"error": oauthprovider.ErrAccessDenied})
		return
	}

	wxProfile := &models.WXProfile{}
	if requester.UnionID != "" {
		gormDB := db.MustGetGorm(c)
		if err = gormDB.Where("unionid = ?", requester.UnionID).First(wxProfile).Error; err != nil {
			logger.Error().Log("msg", "failed to get wxProfile by unionid, just use empty wxProfile.",
				"unionid", requester.UnionID,
				"error", err.Error())

			wxProfile = nil
		}
	}

	serializer := serializers.ProfileSerializer{User: requester, WxProfile: wxProfile}
	c.JSON(http.StatusOK, gin.H{"Data": serializer.Response()})
}

func routeMyGroupsHandler(c *gin.Context) {
	logger := appkitlog.ForceContext(c.Request.Context())
	logger = logger.With("func", "routeMyGroupsHandler")

	requester, err := oauthprovider.GetRequesterFromContext(c)
	if err != nil || requester == nil {
		logger.Error().Log("msg", "unauthorized opeation request, return 401")
		c.JSON(http.StatusUnauthorized, gin.H{"error": oauthprovider.ErrAccessDenied})
		return
	}

	var groups []models.Group
	// user := models.User{}

	// db.First(&user, "id = ?", requester.ID)
	gormDB := db.MustGetGorm(c)

	if err := gormDB.Model(requester).Related(&groups, "Groups").Error; err != nil {
		logger.Error().Log("msg", "get user groups failed")
		c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}

	c.JSON(http.StatusOK, gin.H{"Data": groups})
}

//
type groupQueryParams struct {
	Keyword string `json:"keyword"`

	Page     int `json:"page"`
	PageSize int `json:"page_size"`
}

func getGroupQueryParams(ctx *gin.Context) (params groupQueryParams) {
	// Keyword
	params.Keyword = ctx.Query("keyword")

	//
	page, err := strconv.Atoi(ctx.Query("page"))
	if err != nil {
		page = 0
	}
	params.Page = page

	pageSize, err := strconv.Atoi(ctx.Query("page_size"))
	if err != nil {
		//
		pageSize = common.DEFAULT_PAGESIZE
	}
	params.PageSize = pageSize

	return
}

func scopeGroupsByKeyword(keyword string) func(db *gorm.DB) *gorm.DB {
	return func(db *gorm.DB) *gorm.DB {
		if len(keyword) == 0 {
			return db
		}

		schema := "%" + keyword + "%"

		return db.Where("name LIKE ? OR description LIKE ?", schema, schema)
	}
}

func routeGetGroupsHandler(c *gin.Context) {
	logger := appkitlog.ForceContext(c.Request.Context())
	logger = logger.With("func", "routeGetGroupsHandler")

	requester, err := oauthprovider.GetRequesterFromContext(c)
	if err != nil || requester == nil || requester.Role != models.ROLE_ADMIN {
		logger.Error().Log("msg", "unauthorized opeation request, return 401")
		c.JSON(http.StatusUnauthorized, gin.H{"error": oauthprovider.ErrAccessDenied})
		return
	}

	params := getGroupQueryParams(c)

	gormDB := db.MustGetGorm(c)

	scope := gormDB.Scopes(
		scopeGroupsByKeyword(params.Keyword),
	)

	groupCollections, err := uc.QueryGroupsCollection(gormDB, func(db *gorm.DB) *gorm.DB {
		return common.Pagination(scope, params.Page, params.PageSize)
	})

	groups, err := uc.QueryGroups(gormDB, func(db *gorm.DB) *gorm.DB {
		return scope
	})
	count := len(groups)

	if err != nil {
		panic(err)
	}

	p := common.NewPage(count, params.Page, params.PageSize)

	//
	pagedGroupCollection := uc.PagedGroups{
		GroupsCollection: groupCollections,
		Page:             p,
	}

	c.JSON(http.StatusOK, gin.H{"Data": pagedGroupCollection})
}

func routeGetOneGroupHandler(c *gin.Context) {
	logger := appkitlog.ForceContext(c.Request.Context())
	logger = logger.With("func", "routeGetOneGroupHandler")

	requester, err := oauthprovider.GetRequesterFromContext(c)
	if err != nil || requester == nil {
		logger.Error().Log("msg", "get requester from context failed",
			"error", err.Error())
		c.JSON(http.StatusUnauthorized, gin.H{"error": oauthprovider.ErrAccessDenied})
		return
	}

	// get group ID
	gid, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		logger.Error().Log("msg", "get param ID from route failed",
			"error", err.Error())
		c.JSON(http.StatusUnprocessableEntity, gin.H{"error": err.Error()})
		return
	}
	logger.Debug().Log("groupID", gid)

	// get group object
	gormDB := db.MustGetGorm(c)
	group := models.Group{}
	group.ID = uint(gid)
	if err := gormDB.Where("id = ?", gid).First(&group).Error; err != nil {
		logger.Error().Log("msg", "get group from DB failed",
			"error", err.Error())
		c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}

	c.JSON(http.StatusOK, gin.H{"Data": group})
}

func routeNewOrUpdateGroupHandler(c *gin.Context) {
	logger := appkitlog.ForceContext(c.Request.Context())
	logger = logger.With("func", "routeNewGroupHandler")

	requester, err := oauthprovider.GetRequesterFromContext(c)
	if err != nil || requester == nil {
		logger.Error().Log("msg", "unauthorized operation")
		c.JSON(http.StatusUnauthorized, gin.H{"error": oauthprovider.ErrAccessDenied})
		return
	}

	// get input profile data
	groupProfile := models.Group{}
	if err := c.BindJSON(&groupProfile); err != nil {
		logger.Error().Log("msg", "invalid group profile parameters",
			"error", err.Error(),
		)
		c.JSON(http.StatusUnprocessableEntity, gin.H{"error": err.Error()})
		return
	}

	// create new profile
	gormDB := db.MustGetGorm(c)

	// check whether gid already exist
	existingGroup := models.Group{}
	if err := gormDB.Where("gid = ?", groupProfile.Gid).First(&existingGroup).Error; err != nil {
		if gorm.IsRecordNotFoundError(err) {
			//
			groupProfile.CreatedBy = requester.ID
			if err := gormDB.Create(&groupProfile).Error; err != nil {

				logger.Error().Log("msg", "create group profile failed",
					"error", err.Error(),
				)
				c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
				return
			}

			c.JSON(http.StatusCreated, gin.H{"Data": groupProfile})
			return

		} else {
			logger.Error().Log("msg", "find group by gid failed",
				"gid", existingGroup.Gid,
				"error", err.Error(),
			)
			c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
			return
		}
	}

	existingGroup.Name = groupProfile.Name
	existingGroup.Type = groupProfile.Type
	existingGroup.Description = groupProfile.Description

	if err := gormDB.Save(&existingGroup).Error; err != nil {
		logger.Error().Log("msg", "Update group by gid failed",
			"gid", existingGroup.Gid,
			"ID", existingGroup.ID,
			"error", err.Error(),
		)
		c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}

	c.JSON(http.StatusOK, gin.H{"Data": existingGroup})
}

func routeUpdateOneGroupHandler(c *gin.Context) {
	logger := appkitlog.ForceContext(c.Request.Context())
	logger = logger.With("func", "routeUpdateOneGroupHandler")

	requester, err := oauthprovider.GetRequesterFromContext(c)
	if err != nil || requester == nil {
		logger.Error().Log("msg", "get requester from context failed",
			"error", err.Error())
		c.JSON(http.StatusUnauthorized, gin.H{"error": oauthprovider.ErrAccessDenied})
		return
	}

	// get Book ID
	gid, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		logger.Error().Log("msg", "get param ID from route failed",
			"error", err.Error())
		c.JSON(http.StatusUnprocessableEntity, gin.H{"error": err.Error()})
		return
	}
	logger.Debug().Log("gid", gid)

	// get book object
	gormDB := db.MustGetGorm(c)

	group := models.Group{}
	if err := gormDB.Where("id = ?", gid).First(&group).Error; err != nil {
		logger.Error().Log("msg", "get group from DB failed",
			"id", gid,
			"error", err.Error())
		c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}

	// get input profile data
	if err := c.BindJSON(&group); err != nil {
		logger.Error().Log("msg", "get updated profile from input failed",
			"error", err.Error())
		c.JSON(http.StatusUnprocessableEntity, gin.H{"error": err.Error()})
		return
	}

	if err := gormDB.Save(&group).Error; err != nil {
		logger.Error().Log("msg", "update book profile failed",
			"error", err.Error())
		c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}

	c.JSON(http.StatusOK, gin.H{"Data": group})
}

//
type groupAttachParams struct {
	Event   string `binding:"required"`
	Comment string

	UserID int
}

func routeAttachToGroupHandler(c *gin.Context) {
	logger := appkitlog.ForceContext(c.Request.Context())
	logger = logger.With("func", "routeAttachToGroupHandler")

	requester, err := oauthprovider.GetRequesterFromContext(c)
	if err != nil || requester == nil {
		logger.Error().Log("msg", "get requester from context failed",
			"error", err.Error())
		c.JSON(http.StatusUnauthorized, gin.H{"error": oauthprovider.ErrAccessDenied})
		return
	}

	// get group
	groupID, err := strconv.Atoi(c.Param("group_id"))
	if err != nil {
		logger.Error().Log("msg", "get param ID from route failed",
			"error", err.Error())
		c.JSON(http.StatusUnprocessableEntity, gin.H{"error": err.Error()})
		return
	}
	logger.Debug().Log("group_id", groupID)

	// get book object
	gormDB := db.MustGetGorm(c)

	group := models.Group{}
	if err := gormDB.Where("id = ?", groupID).First(&group).Error; err != nil {
		logger.Error().Log("msg", "get group from DB failed",
			"id", groupID,
			"error", err.Error())
		c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}

	// get input profile data
	attachParams := groupAttachParams{}
	if err := c.BindJSON(&attachParams); err != nil {
		logger.Error().Log("msg", "get attach params from input failed",
			"error", err.Error())
		c.JSON(http.StatusUnprocessableEntity, gin.H{"error": err.Error()})
		return
	}

	// only ADMIN can attach/detatch other user to a group
	// uid := requester.ID
	// if attachParams.UserID > 0 {
	// 	uid = uint(attachParams.UserID)
	// }

	// if uid != requester.ID && requester.Role != models.ROLE_ADMIN {
	// 	logger.Error().Log("msg", "no permission to attach other user to group",
	// 		"userID", uid,
	// 		"gid", groupID,
	// 		"error", err.Error())
	// 	c.JSON(http.StatusUnauthorized, gin.H{"error": "no permission to attach other user to a group"})
	// 	return
	// }

	//
	if attachParams.Event == models.GROUP_EVENT_ATTACH {
		if err := gormDB.Model(&group).Association("Members").Append(requester).Error; err != nil {
			logger.Error().Log("msg", "associate user to group failed",
				"groupID", group.ID,
				"userID", requester.ID,
				"error", err.Error())
			c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
			return
		}
	} else if attachParams.Event == models.GROUP_EVENT_DETATCH {
		if err := gormDB.Model(&group).Association("Members").Delete(requester).Error; err != nil {
			logger.Error().Log("msg", "delete user from group failed",
				"groupID", group.ID,
				"userID", requester.ID,
				"error", err.Error())
			c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
			return
		}
	} else {
		c.JSON(http.StatusUnprocessableEntity, gin.H{"error": "not supported event"})
		return
	}

	// update event log
	eventLog := models.GroupEventLog{}
	eventLog.Event = attachParams.Event
	eventLog.Gid = group.Gid
	eventLog.Comment = attachParams.Comment
	eventLog.UserID = requester.ID
	eventLog.GroupID = group.ID
	eventLog.RequestedByUserID = requester.ID

	if err := gormDB.Save(&eventLog).Error; err != nil {
		logger.Error().Log("msg", "save event log failed",
			"groupID", group.ID,
			"userID", requester.ID,
			"requesterID", requester.ID,
			"error", err.Error())
		// NOTE: DO NOT retuen error here!
	}

	c.JSON(http.StatusOK, gin.H{"Data": group})
}

func routeGetOneUserProfileHandler(c *gin.Context) {
	logger := appkitlog.ForceContext(c.Request.Context())
	logger = logger.With("func", "routeGetOneUserProfileHandler")

	requester, err := oauthprovider.GetRequesterFromContext(c)
	if err != nil || requester == nil {
		logger.Error().Log("msg", "unauthorized opeation request, return 401")
		c.JSON(http.StatusUnauthorized, gin.H{"error": oauthprovider.ErrAccessDenied})
		return
	}

	if requester.Role != models.ROLE_ADMIN {
		logger.Error().Log("msg", "operation only allowed for admin, return 403")
		c.JSON(http.StatusForbidden, gin.H{"error": errors.New("Only allowed for admin.")})
		return
	}

	//
	userID, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		logger.Error().Log("msg", "get user ID failed",
			"error", err.Error(),
		)
		c.JSON(http.StatusUnprocessableEntity, gin.H{"error": err.Error(), "msg": "输入参数错误"})
		return
	}

	user := models.User{}
	gormDB := db.MustGetGorm(c)
	if err = gormDB.Where("id = ?", userID).First(&user).Error; err != nil {
		logger.Error().Log("msg", "get user by ID failed",
			"error", err.Error(),
			"user ID", userID,
		)
		c.JSON(http.StatusInternalServerError, gin.H{"error": err})
		return
	}

	wxProfile := &models.WXProfile{}
	if user.UnionID != "" {
		if err = gormDB.Where("unionid = ?", user.UnionID).First(wxProfile).Error; err != nil {
			logger.Error().Log("msg", "failed to get wxProfile by unionid, just use empty wxProfile.",
				"unionid", requester.UnionID,
				"error", err.Error())

			wxProfile = nil
		}
	}

	serializer := serializers.ProfileSerializer{User: &user, WxProfile: wxProfile}
	c.JSON(http.StatusOK, gin.H{"Data": serializer.Response()})
}

//
type userPatchParameters struct {
	// basic account info
	Role string `json:"role"`

	// other profile
	Email        string `json:"email"`
	HeadPortrait string `json:"head_portrait"`
	Remark       string `json:"remark"`

	// SMS related
	Mobile string `json:"mobile"`
}

func routePatchOneUserHandler(c *gin.Context) {
	logger := appkitlog.ForceContext(c.Request.Context())
	logger = logger.With("func", "routePatchOneUserHandler")

	requester, err := oauthprovider.GetRequesterFromContext(c)
	if err != nil || requester == nil {
		logger.Error().Log("msg", "unauthorized opeation request, return 401")
		c.JSON(http.StatusUnauthorized, gin.H{"error": oauthprovider.ErrAccessDenied})
		return
	}

	params := userPatchParameters{}
	gormDB := db.MustGetGorm(c) //上下文获取Gorm
	userID, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		logger.Error().Log("msg", "get user ID failed",
			"error", err.Error(),
		)
		c.JSON(http.StatusUnprocessableEntity, gin.H{"error": err.Error(), "msg": "用户ID获取失败"})
		return
	}

	user := models.User{}
	if err = gormDB.Where("id = ?", userID).First(&user).Error; err != nil {
		if err == gorm.ErrRecordNotFound {
			logger.Error().Log("msg", "user id not exist",
				"error", "Record Not Found",
				"userID", userID,
			)
			c.JSON(http.StatusUnprocessableEntity, gin.H{"error": "user id not exist"})
			return
		}

		logger.Error().Log("msg", "get user by ID failed",
			"error", err.Error(),
			"user ID", userID,
		)
		c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}

	err = c.Bind(&params)
	if err != nil {
		logger.Error().Log("msg", "bind parameters failed",
			"error", err.Error(),
			"userID", userID,
		)
		c.JSON(http.StatusUnprocessableEntity, gin.H{"error": err.Error(), "msg": "输入参数错误"})
		return
	}

	newUser := models.User{}
	newUser.ID = uint(userID)

	if requester.Role != models.ROLE_ADMIN && requester.ID != newUser.ID {
		logger.Error().Log("msg", "no permission to update user profile",
			"userID", userID,
			"requesterID", requester.ID,
		)
		c.JSON(http.StatusForbidden, gin.H{"error": err.Error(), "msg": "no permission to update user profile"})
		return
	}

	// only admin can update role
	if requester.Role == models.ROLE_ADMIN {
		newUser.Role = params.Role
	}

	newUser.Mobile = params.Mobile
	newUser.HeadPortrait = params.HeadPortrait
	newUser.Email = params.Email
	newUser.Remark = params.Remark

	if err = gormDB.Model(&newUser).Updates(&newUser).Error; err != nil {
		//修改相应字段
		logger.Error().Log("msg", "update user info failed",
			"error", err.Error(),
			"userID", userID,
		)
		c.JSON(http.StatusInternalServerError, gin.H{"error": err})
		return
	}

	serializer := serializers.ProfileSerializer{User: &newUser}
	c.JSON(http.StatusOK, gin.H{"Data": serializer.UserResponse()})
}

func routePostWXProfileHandler(c *gin.Context) {
	logger := appkitlog.ForceContext(c.Request.Context())
	logger = logger.With("func", "routePostWXProfileHandler")

	requester, err := oauthprovider.GetRequesterFromContext(c)
	if err != nil || requester == nil {
		logger.Error().Log("msg", "unauthorized opeation request, return 401")
		c.JSON(http.StatusUnauthorized, gin.H{"error": oauthprovider.ErrAccessDenied})
		return
	}

	gormDB := db.MustGetGorm(c) //上下文获取Gorm
	userID, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		logger.Error().Log("msg", "get user ID failed",
			"error", err.Error(),
		)
		c.JSON(http.StatusUnprocessableEntity, gin.H{"error": err.Error(), "msg": "用户ID获取失败"})
		return
	}

	user := models.User{}
	if err = gormDB.Where("id = ?", userID).First(&user).Error; err != nil {
		if err == gorm.ErrRecordNotFound {
			logger.Error().Log("msg", "user id not exist",
				"error", "Record Not Found",
				"userID", userID,
			)
			c.JSON(http.StatusUnprocessableEntity, gin.H{"error": "user id not exist"})
			return
		}

		logger.Error().Log("msg", "get user by ID failed",
			"error", err.Error(),
			"user ID", userID,
		)
		c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}

	// get wx profile
	wxProfileParam := WxProfileParam{}
	if err := c.BindJSON(&wxProfileParam); err != nil {
		logger.Error().Log("msg", "bind JSON failed",
			"error", err.Error(),
		)
		c.JSON(http.StatusUnprocessableEntity, gin.H{"error": err.Error()})
		return
	}

	wxProfile := models.WXProfile{
		OpenID:     wxProfileParam.OpenID,
		NickName:   wxProfileParam.NickName,
		Sex:        float64(wxProfileParam.Sex),
		City:       wxProfileParam.City,
		Province:   wxProfileParam.Province,
		Country:    wxProfileParam.Country,
		HeadImgUrl: wxProfileParam.HeadImgUrl,
		UnionID:    wxProfileParam.UnionID,
		Language:   wxProfileParam.Language,
	}

	if requester.Role != models.ROLE_ADMIN && requester.ID != user.ID {
		logger.Error().Log("msg", "no permission to update wx profile",
			"userID", userID,
			"requesterID", requester.ID,
		)
		c.JSON(http.StatusForbidden, gin.H{"error": err.Error(), "msg": "no permission to update user profile"})
		return
	}

	// if user.Username != wxProfile.OpenID {
	// 	logger.Error().Log("msg", "no permission to update wx profile",
	// 		"userID", userID,
	// 		"username", user.Username,
	// 		"openid", wxProfile.OpenID,
	// 		"requesterID", requester.ID,
	// 	)
	// 	c.JSON(http.StatusForbidden, gin.H{"error": err.Error(), "msg": "no permission to update user profile"})
	// 	return
	// }

	// get wxProfile by openid
	tx := gormDB.Begin()
	existingWxProfile := models.WXProfile{}
	if err := gormDB.Where("openid = ?", wxProfile.OpenID).First(&existingWxProfile).Error; err != nil {
		if !gorm.IsRecordNotFoundError(err) {
			logger.Error().Log("msg", "get wx profile by openid failed",
				"openid", wxProfile.OpenID,
				"requesterID", requester.ID,
			)
			c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error(), "msg": "get wx profile by openid failed"})
			return
		}

		// record not found, create new
		// existingWxProfile.OpenID = wxProfileParam.OpenID
		// existingWxProfile.NickName = wxProfileParam.NickName
		// existingWxProfile.Sex = float64(wxProfileParam.Sex)
		// existingWxProfile.City = wxProfileParam.City
		// existingWxProfile.Province = wxProfileParam.Province
		// existingWxProfile.Country = wxProfileParam.Country
		// existingWxProfile.HeadImgUrl = wxProfileParam.HeadImgUrl
		// existingWxProfile.UnionID = wxProfileParam.UnionID
		// existingWxProfile.Language = wxProfileParam.Language

		// create new profile & attach to user
		if err := tx.Create(&existingWxProfile).Error; err != nil {
			tx.Rollback()
			logger.Error().Log("msg", "create wx profile by openid failed",
				"openid", wxProfile.OpenID,
				"requesterID", requester.ID,
			)
			c.JSON(http.StatusUnprocessableEntity, gin.H{"error": err.Error(), "msg": "create wx profile by openid failed"})
			return
		}
	}

	// update wxProfile
	wxProfile.ID = existingWxProfile.ID
	if err := tx.Save(&wxProfile).Error; err != nil {
		tx.Rollback()
		logger.Error().Log("msg", "update wx profile by openid failed",
			"openid", wxProfile.OpenID,
			"requesterID", requester.ID,
			"error", err.Error(),
		)
		c.JSON(http.StatusForbidden, gin.H{"error": err.Error(), "msg": "update wx profile by openid failed"})
		return
	}

	// update user unionid
	user.UnionID = wxProfile.UnionID
	user.NickName = wxProfile.NickName
	user.HeadPortrait = wxProfile.HeadImgUrl
	if err := tx.Save(&user).Error; err != nil {
		tx.Rollback()
		logger.Error().Log("msg", "update user profile failed",
			"userID", user.ID,
			"wxProfileID", wxProfile.ID,
			"error", err.Error(),
		)
		c.JSON(http.StatusUnprocessableEntity, gin.H{"error": err.Error(), "msg": "update wx profile by openid failed"})
		return
	}

	if err := tx.Commit().Error; err != nil {
		logger.Error().Log("msg", "commit changes to DB failed",
			"error", err.Error(),
		)

		c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}

	serializer := serializers.ProfileSerializer{User: &user}
	c.JSON(http.StatusOK, gin.H{"Data": serializer.UserResponse()})
}

func getQiniuUploadTokenHandler(config *appconfig.AppConfig) func(c *gin.Context) {
	return func(c *gin.Context) {
		// Keyword
		fileName := c.Query("keyword")

		userImage := "https://images.noebook.net" //common.UserHeadPortraitUrl //用户头像url前缀域名
		requester, err := oauthprovider.GetRequesterFromContext(c)
		if err != nil {
			c.JSON(http.StatusUnprocessableEntity, gin.H{"error": err.Error(), "msg": "Token解析错误"})
			return
		}

		gormDB := db.MustGetGorm(c)
		err = gormDB.Where("id = ?", requester.ID).Error
		if err != nil {
			c.JSON(http.StatusUnprocessableEntity, gin.H{"error": err.Error(), "msg": "Invalid user"})
			return
		}

		//
		uploadToken := qiniup.QiNiuUploadSimpleToken(&config.Qiniu)
		if len(fileName) > 0 {
			uploadToken = qiniup.QiNiuUploadToken(&config.Qiniu, fileName)
		}

		// 返回上传凭证值
		c.JSON(http.StatusOK, gin.H{"status": "OK", "upToken": uploadToken, "userimgurl": userImage})
		return
	}
}
