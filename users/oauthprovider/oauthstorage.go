package oauthprovider

import (
	"fmt"
	"reflect"
	"time"

	"gitlab.com/moakap/mapi/common"

	"github.com/RangelReale/osin"
	"github.com/jinzhu/gorm"
	"github.com/pkg/errors"
	"gitlab.com/moakap/mapi/users/models"

	appkitlog "github.com/theplant/appkit/log"
	appdb "gitlab.com/moakap/mapi/db"
)

var errAuthorizationUnsupported = errors.New("authorization not supported")

// OAuthStorage implements osin.Storage interface.
type OAuthStorage struct {
	db     *gorm.DB
	logger appkitlog.Logger
}

func asOsinAccessData(a models.AccessData) *osin.AccessData {
	now := time.Now()
	return &osin.AccessData{
		Client:       osin.Client(a.Client),
		AccessToken:  a.AccessToken,
		RefreshToken: a.RefreshToken,
		Scope:        a.Scope,
		RedirectUri:  a.Client.RedirectURI,
		CreatedAt:    now,
		UserData:     a.User,
		ExpiresIn:    int32(a.ExpiresAt.Sub(now) / time.Second),
	}
}

// Clone the storage if needed. For example, using mgo, you can clone the session with session.Clone
// to avoid concurrent access problems.
// This is to avoid cloning the connection at each method access.
// Can return itself if not a problem.
func (s OAuthStorage) Clone() osin.Storage {
	return s
}

// Close the resources the Storage potentially holds (using Clone for example)
func (s OAuthStorage) Close() {
}

func getClientFromDB(db *gorm.DB, clientID string) (models.Client, error) {
	var c models.Client
	err := errors.Wrapf(db.Where("client_id = ?", clientID).Find(&c).Error, "OAuthStorage.GetClient no client with id %s", clientID)

	return c, err
}

// GetClient loads the client by id (client_id)
func (s OAuthStorage) GetClient(id string) (osin.Client, error) {
	// var c models.Client
	// err := errors.Wrapf(s.db.Where("client_id = ?", id).Find(&c).Error, "OAuthStorage.GetClient no client with id %s", id)

	// return c, err

	return getClientFromDB(s.db, id)
}

// SaveAuthorize not supported (codes are currently only generated
// when requesting a password reset, OAuth clients can't directly ask
// for a code).

// SaveAuthorize saves authorize data.
func (s OAuthStorage) SaveAuthorize(auth *osin.AuthorizeData) error {
	s.logger.Warn().Log(
		"msg", "SaveAuthorize unsupported",
		"context", "oauthprovider.OAuthStorage.SaveAuthorize",
	)

	// storedClient, err := getClientFromDB(auth.Client.GetId())
	// if err := nil {
	// 	s.logger.Error().Log(
	// 		"msg", "Get client failed.",
	// 		"client_id", auth.Client.GetId(),
	// 		"error", err.Error(),
	// 	)

	// 	return err
	// }

	// authorizeData := models.AuthorizeData{
	// 	ClientID:  storedClient.ID,
	// 	Client:    storedClient,
	// 	Code:      auth.Code,
	// 	ExpiresAt: auth.ExpireAt(),
	// 	Scope:     auth.Scope,
	// }
	return errAuthorizationUnsupported
}

func (s OAuthStorage) LoadAuthorize(code string) (*osin.AuthorizeData, error) {
	authData := &models.AuthorizeData{}
	if err := s.db.Preload("Client").Preload("User").Where("code = ?", code).Find(&authData).Error; err != nil {
		return nil, errors.Wrapf(err, "error loading authorization code %s", code)
	}

	data := osin.AuthorizeData{
		Client: authData.Client,
		Code:   code,
		// time.Since is negative for times in the future
		ExpiresIn:   -int32(time.Since(authData.ExpiresAt) / time.Second),
		Scope:       authData.Scope,
		RedirectUri: authData.Client.RedirectURI,
		State:       "",
		CreatedAt:   authData.CreatedAt,
		UserData:    authData.User,
	}

	return &data, nil
}

func (s OAuthStorage) RemoveAuthorize(code string) error {
	err := s.db.Where("code = ?", code).Delete(&models.AuthorizeData{}).Error

	return errors.Wrapf(err, "error removing authorization code", code)
}

// SaveAccess stores data in AccessData into an AccessToken in the database
func (s OAuthStorage) SaveAccess(access *osin.AccessData) error {
	user := access.UserData.(models.User)

	accessData, err := s.findAccessDataByAccessToken(access.AccessToken)
	if err == gorm.ErrRecordNotFound {
		client := access.Client.(models.Client)

		accessData = models.AccessData{
			User:         user,
			AccessToken:  access.AccessToken,
			RefreshToken: access.RefreshToken,
			ClientID:     client.ID,
			ExpiresAt:    access.ExpireAt(),
			Scope:        access.Scope,
		}

		appdb.ExecInTransaction(s.db, func(db *gorm.DB) error {
			err = db.Save(&accessData).Error

			if err == nil {
				s.logger.Info().Log("msg", fmt.Sprintf("deleting auth codes for user %d", user.ID))
				err = db.Where("user_id = ?", user.ID).Delete(&models.AuthorizeData{}).Error
			}

			return err
		})

	}

	return errors.Wrap(err, "OAuthStorage.SaveAccess error")
}

// LoadAccess retrieves access data by token lookup in DB.
func (s OAuthStorage) LoadAccess(token string) (data *osin.AccessData, err error) {
	at, err := s.findAccessDataByAccessToken(token)
	if err != nil {
		return nil, errors.Wrap(err, "OAuthStorage.LoadAccess findToken failed")
	}

	data = asOsinAccessData(at)
	if data.IsExpired() {
		s.logger.Info().Log(
			"msg", fmt.Sprintf("LoadAccess failed for token %s, expired", token),
			"context", "oauthprovider.OAuthStorage.LoadAccess",
			"reason", "expired",
			"metric", "oauthprovider.OAuthStorage.tokenExpired",
			"value", 1,
		)
		return nil, errors.New(osin.E_ACCESS_DENIED)
	}

	return data, nil
}

// RemoveAccess revokes or deletes AccessToken from DB.
func (s OAuthStorage) RemoveAccess(token string) error {
	at, err := s.findAccessDataByAccessToken(token)

	if err != nil {
		return errors.Wrap(err, "OAuthStorage.RemoveAccess findToken failed")
	}

	return errors.Wrap(s.db.Delete(at).Error, "OAuthStorage.RemoveAccess db.Delete failed")
}

func findTokenWithScope(scope *gorm.DB) (models.AccessData, error) {
	var at models.AccessData
	err := scope.Preload("Client").Preload("User").Find(&at).Error

	// Tokens have a DB constraint on client, so we shouldn't end up
	// with missing clients.
	if err == nil && at.Client.ID == 0 {
		err = errors.New("access token has no client")
	}

	return at, err
}

func (s OAuthStorage) findAccessDataByAccessToken(accessToken string) (models.AccessData, error) {
	db := s.db.Where("access_token = ?", accessToken)
	return findTokenWithScope(db)
}

func (s OAuthStorage) findAccessDataByRefreshToken(refreshToken string) (models.AccessData, error) {
	db := s.db.Where("refresh_token = ?", refreshToken)
	return findTokenWithScope(db)
}

// LoadRefresh creates new AccessData given a refresh token
func (s OAuthStorage) LoadRefresh(token string) (*osin.AccessData, error) {
	at, err := s.findAccessDataByRefreshToken(token)
	if err != nil {
		return nil, errors.Wrap(err, "OAuthStorage.LoadRefresh findAccessDataByRefreshToken failed")
	}

	return &osin.AccessData{
		Client:       osin.Client(at.Client),
		RefreshToken: at.RefreshToken,
		AccessData:   asOsinAccessData(at),
		UserData:     at.User,
		// Note the scope is not carried over from the previous
		// token. This is fine for the moment, we only have a single
		// scope 'password.change' and we *don't want* to carry this
		// scope to the refreshed token! If/when we introduce more
		// scopes, the `password.change` scope should be *stripped*.
	}, nil
}

// RemoveRefresh invalidates a refresh token
func (s OAuthStorage) RemoveRefresh(token string) error {
	at, err := s.findAccessDataByRefreshToken(token)

	if err != nil {
		return errors.Wrap(err, "OAuthStorage.RemoveRefresh findAccessDataByRefreshToken failed")
	}

	return errors.Wrap(s.db.Delete(at).Error, "OAuthStorage.RemoveRefresh db.Delete failed")
}

// process loads user with email/password in access request, and
// inserts an existing access token into the access request if there
// is one.
// TODO: should this really return the same token for multiple requests?
func (s OAuthStorage) process(ar *osin.AccessRequest, ip string) error {
	user, err := models.UserAuthenticate(s.db, ar.Username, ar.Password)
	if err == models.ErrInvalidMobileOrPassword {
		return errors.Wrap(err, "OAuthStorage.process failed")
	} else if err != nil {
		return err
	}

	//insert AuditLog
	auditlog := models.AuditLog{
		UserID:    user.ID,
		IPAddress: ip,
		Action:    common.GetAuditAction(common.AUDIT_ACTION_LOGIN, ar.Client.GetId()),
		Details:   "用户登陆客户端记录",
	}
	if err := s.db.Create(&auditlog).Error; err != nil {
		s.logger.Error().Log(
			"msg", fmt.Sprintf("insert  error: %s", err),
			"message", "insert AuditLog is failed ",
			"err", err,
		)
	}

	ar.UserData = user
	_, ok := ar.Client.(models.Client)
	if !ok {
		panic(fmt.Errorf("expected Client, got %v", reflect.ValueOf(ar.Client)))
	}

	ar.Authorized = true

	return nil
}
