// Package oauthprovider is an OAuth2 API "provider" that exposes functionality such as:
// - Swapping mobile/password for access token
// - Refreshing access token
// - Middleware for validating access token
package oauthprovider

import (
	"errors"
	"fmt"
	"net/http"
	"time"

	"gitlab.com/moakap/mapi/appconfig"
	"gitlab.com/moakap/mapi/users/jwtgenerator"
	"gitlab.com/moakap/mapi/users/models"
	"gitlab.com/moakap/mapi/users/serializers"

	"github.com/RangelReale/osin"
	"github.com/gin-gonic/gin"

	appkitlog "github.com/theplant/appkit/log"
	appdb "gitlab.com/moakap/mapi/db"
)

// jwtGen is the JWT generator for authorization token.
type jwtGen struct {
	jwtgenerator.Generator
}

// GenerateAccessToken generates JWT access token and refresh token using *ES512* algorithm.
func (a *jwtGen) GenerateAccessToken(data *osin.AccessData, generateRefreshToken bool) (accessToken string, refreshToken string, err error) {
	user, ok := data.UserData.(models.User)

	if !ok {
		err = errors.New("invalid user data")
		return
	}

	client, ok := data.Client.(models.Client)

	if !ok {
		err = errors.New("invalid client")
		return
	}

	accessToken, err = a.Generator.GenerateAccessToken(client.ClientID, user.ID, data.CreatedAt, data.ExpireAt(), data.Scope)

	if err != nil {
		return
	}

	// should generate refresh token or not?
	if !generateRefreshToken {
		return
	}

	refreshToken = a.GenerateRefreshToken()

	return
}

// ErrAccessDenied can be used by HTTP handlers to signal that the
// request failed due to authorization issues
var ErrAccessDenied = errors.New(osin.E_ACCESS_DENIED)

// Provider represents an OAuth provider with internal storage for API
// data
type Provider struct {
	osin.Server
}

// MountTo mounts OAuth "token" endpoint to "/token" on router
func MountTo(router *gin.RouterGroup, appConfig *appconfig.AppConfig) *Provider {
	cfg := osin.NewServerConfig()
	cfg.AllowGetAccessRequest = false
	cfg.AllowClientSecretInParams = true
	cfg.ErrorStatusCode = 400
	cfg.AllowedAccessTypes = []osin.AccessRequestType{osin.PASSWORD, osin.REFRESH_TOKEN, osin.AUTHORIZATION_CODE}

	// Try to avoid making daily users have to refresh their token the
	// first time they use the app every day:
	// https://qortex.com/theplant#groups/560b63da8d93e34b8500da28/entry/582280248d93e32abc1a6de3
	cfg.AccessExpiration = int32(26 * time.Hour / time.Second)

	server := osin.NewServer(cfg, nil)

	jwtGenerator, err := jwtgenerator.New(appConfig.JWT, appConfig.APIRoot())
	if err != nil {
		panic(err)
	}

	server.AccessTokenGen = &jwtGen{
		jwtGenerator,
	}

	// FIXME default logger usage
	server.Storage = OAuthStorage{nil, appkitlog.Default()}

	provider := Provider{*server}

	// Access token endpoint
	router.POST("/token", provider.ginTokenEndpoint)
	router.POST("/verification_code", grantVerificationCodeHandler(appConfig))

	//忘记密码
	router.POST("/set_password", routePatchPasswordHandler)

	return &provider
}

type patchPasswordParameters struct {
	Username         string `json:"username"`
	Password         string `json:"password"`
	VerificationCode string `json:"verification_code"`
}

//修改密码
func routePatchPasswordHandler(c *gin.Context) {
	logger := appkitlog.ForceContext(c.Request.Context())
	logger = logger.With("func", "routePatchPasswordHandler")

	params := patchPasswordParameters{}
	if err := c.BindJSON(&params); err != nil {
		logger.Error().Log("msg", "bind parameters failed",
			"error", err.Error(),
		)
		c.JSON(http.StatusUnprocessableEntity, gin.H{"error": err.Error(), "msg": "输入参数错误"})
		return
	}

	user := models.User{}
	gormDB := appdb.MustGetGorm(c)
	//1.检查用户是否存在
	if err := gormDB.Where("username = ?", params.Username).First(&user).Error; err != nil {
		logger.Error().Log("msg", "get user by username failed",
			"username", params.Username,
			"error", err.Error(),
		)
		c.JSON(http.StatusUnprocessableEntity, gin.H{"error": errors.New("username does not exist").Error()})
		return
	}

	//2.校验用户验证码时效
	if user.SmsVerificationCodeSentAt != nil {
		duration := time.Now().Sub(*user.SmsVerificationCodeSentAt)
		if duration.Seconds() > 300 {
			logger.Error().Log("msg", "verification code expired",
				"username", params.Username,
			)
			c.JSON(http.StatusForbidden, gin.H{"error": errors.New("verification code expired").Error()})
			return
		}
	}

	//3.校验用户验证码是否正确
	if user.SmsVerificationCode != params.VerificationCode { //校验用户输入的验证码
		logger.Error().Log("msg", "incorrect verification code",
			"username", params.Username,
			"expected code", user.SmsVerificationCode,
			"got", params.VerificationCode,
		)
		c.JSON(http.StatusForbidden, gin.H{"error": errors.New("incorrect verifaction code").Error()})
		return
	}

	//4.用户密码加密
	if err := user.SetPassword(params.Password); err != nil {
		logger.Error().Log("msg", "set encrypted password failed",
			"username", params.Username,
			"error", err.Error(),
		)
		c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}

	//5.更改并保存用户信息
	if err := gormDB.Save(&user).Error; err != nil {
		logger.Error().Log("msg", "save updated password to DB failed",
			"username", params.Username,
			"error", err.Error(),
		)
		c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}

	//6.返回修改结果
	serializer := serializers.ProfileSerializer{User: &user}
	c.JSON(http.StatusOK, gin.H{"status": "OK", "data": serializer.UserResponse()})
}

func grantVerificationCodeHandler(appConfig *appconfig.AppConfig) func(ctx *gin.Context) {
	return func(ctx *gin.Context) {
		gormDB := appdb.MustGetGorm(ctx)
		mobile := ctx.Query("mobile")
		if mobile == "" {
			ctx.JSON(http.StatusUnprocessableEntity, gin.H{"error": errors.New("输入手机号码错误")})
			return
		}

		verticateStr, err := models.RequestVerificationCode(appConfig, gormDB, mobile)
		if err != nil {
			ctx.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
			return
		}

		// add verificate as value return
		ctx.JSON(http.StatusOK, gin.H{"status": "OK", "verificate": verticateStr})
	}
}

func (p *Provider) ginTokenEndpoint(ctx *gin.Context) {
	logger := appkitlog.ForceContext(ctx.Request.Context()).With("context", "oauthprovider.endpoint")

	w := ctx.Writer
	r := ctx.Request

	resp := p.NewResponse()
	defer resp.Close()
	s := storage(ctx)
	resp.Storage = s
	if ar := p.HandleAccessRequest(resp, r); ar != nil {
		// FIXME push grant_type, client ID, user ID into logger tag space
		// https://github.com/theplant/appkit/issues/14
		clientID := ar.Client.GetId()

		logger = logger.With(
			"grant_type", ar.Type,
			"client_id", clientID,
		)

		var err error
		switch ar.Type {
		case osin.PASSWORD:
			logger.Debug().Log("msg", "osin.Type = password")
			err = s.process(ar, ctx.ClientIP())
			if err != nil {
				logger.Error().Log(
					"msg", fmt.Sprintf("Error processing OAuth token request: %s", err),
					"err", err,
					"during", "oauthprovider.OAuthStorage.process",
				)
			}
		case osin.REFRESH_TOKEN:
			fallthrough
		case osin.AUTHORIZATION_CODE:
			ar.Authorized = true
		}

		p.FinishAccessRequest(resp, r, ar)

		if ar.Authorized {
			userID := ar.UserData.(models.User).ID
			logger.Info().Log(
				"user_id", userID,
				"msg", fmt.Sprintf("oauth access token issued via %v grant for user %d to client %v", ar.Type, userID, clientID),
			)
		}
	}
	if resp.IsError {
		if resp.InternalError != nil {
			logger.Error().Log(
				"during", "HandleAccessRequest",
				"err", resp.InternalError,
				"msg", fmt.Sprintf("internal error handling OAuth token request: %s", resp.InternalError),
			)
		} else {
			logger.Error().Log(
				"msg", fmt.Sprintf("error handling OAuth token request: %s", resp.ErrorId),
				"oautherror", resp.ErrorId,
			)
		}
	}

	osin.OutputJSON(resp, w, r)
}

// Context is a wrapper for gin.Context to provide access to
// OAuth2-specific data stored in the context
type Context struct {
	gin.Context
}

const accessDataKey = "oauth:access_data"

// AccessToken extracts the access token from a request context
func (c Context) AccessToken() (string, error) {
	accessData, err := c.accessData()
	if err != nil || accessData == nil {
		return "", errors.New("no access token")
	}
	return accessData.AccessToken, nil
}

// UserData extracts the user data from a request context
func (c Context) UserData() (*models.User, error) {
	accessData, err := c.accessData()
	if err != nil || accessData == nil {
		return nil, errors.New("no user data")
	}

	user := accessData.UserData.(models.User)
	return &user, nil
}

// Client extracts the client from a request context
func (c Context) Client() (*models.Client, error) {
	accessData, err := c.accessData()
	if err != nil || accessData == nil {
		return nil, errors.New("no client")
	}

	client := accessData.Client.(models.Client)
	return &client, nil
}

func (c Context) accessData() (*osin.AccessData, error) {
	d, ok := c.Get(accessDataKey)
	if ok {
		accessData := d.(*osin.AccessData)
		return accessData, nil
	}
	return nil, errors.New("no access data")
}

func (p *Provider) auth(c *gin.Context) (accessData *osin.AccessData, err error) {
	if bearer := osin.CheckBearerAuth(c.Request); bearer != nil {
		accessData, err = storage(c).LoadAccess(bearer.Code)
	}
	return
}

// RequireScope is Gin middleware that validates that the access token
// used in the request has the given scope.
//
// Aborts the request with 403 Forbidden if the access token does not
// have the required scope.
func RequireScope(scope string) gin.HandlerFunc {
	return func(ctx *gin.Context) {
		if !hasScope(ctx, scope) {
			appkitlog.ForceContext(ctx.Request.Context()).Warn().Log(
				"msg", fmt.Sprintf("rejected request requiring scope %s", scope),
				"requiredscope", scope,
			)
			ctx.AbortWithStatus(http.StatusForbidden)
		} else {
			ctx.Next()
		}
	}
}

// Note, scope must match exactly, multiple scopes on a token are not
// currently supported
func hasScope(ctx *gin.Context, scope string) bool {
	data, err := Context{*ctx}.accessData()
	return err == nil &&
		data != nil &&
		data.Scope == scope
}

// Authenticator is middleware to process OAuth bearer token into
// context if it is set and valid. Note that it will not abort the
// request if the token is absent, as this is the responsibility of
// each individual resource/handler.
func (p *Provider) Authenticator(context *gin.Context) {
	logger := appkitlog.ForceContext(context.Request.Context())

	accessData, err := p.auth(context)
	if err != nil {

		logger.Error().Log(
			"msg", fmt.Sprintf("auth error: %s", err),
			"during", "oauthprovider.Provider.auth",
			"err", err,
		)
		context.Error(err)
		// Needs to set authentication header
		context.AbortWithStatus(http.StatusUnauthorized)
	} else {
		context.Set(accessDataKey, accessData)
		context.Next()
	}
}

func storage(c *gin.Context) OAuthStorage {
	gormDB := appdb.MustGetGorm(c)
	logger := appkitlog.ForceContext(c).With(
		"context", "oauthprovider",
	)

	storage := OAuthStorage{gormDB, logger}
	return storage
}

func NewOsinContext(ctx *gin.Context) Context {
	return Context{*ctx}
}

func GetRequesterFromContext(ctx *gin.Context) (*models.User, error) {
	oc := NewOsinContext(ctx)
	return oc.UserData()
}
