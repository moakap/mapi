package models

import "github.com/jinzhu/gorm"

// Client is OAuth API Client Application
type Client struct {
	gorm.Model

	ClientID    string `gorm:"unique_index;not null"`
	Secret      string `gorm:"not null"`
	RedirectURI string `gorm:"not null"`
}

// ===================== osin.Client interface methods =======================
// GetId is part of osin.Client
func (c Client) GetId() string {
	return c.ClientID
}

// GetSecret is part of osin.Client
func (c Client) GetSecret() string {
	return c.Secret
}

// GetRedirectUri is part of osin.Client
func (c Client) GetRedirectUri() string {
	return c.RedirectURI
}

// GetUserData is part of osin.Client
func (c Client) GetUserData() interface{} {
	return c.ID
}
