package models

import (
	"time"

	"github.com/jinzhu/gorm"

	appkitlog "github.com/theplant/appkit/log"
	appdb "gitlab.com/moakap/mapi/db"
)

// AuthorizeData is part of OAuth2 Authorization Code Grant flow
// (RFC6749 Section 4.1). Clients with an authorization code can swap
// the code for an access token. Note, authorization requests are not
// currently supported, the only way to *generate* an authorization
// code is to request a password reset email, and the code is included
// in the email text.
type AuthorizeData struct {
	gorm.Model

	Code      string `gorm:"unique_index;not null"`
	ExpiresAt time.Time
	Scope     string `gorm:"not null"`

	User   User
	UserID uint `gorm:"not null"`

	Client   Client
	ClientID uint `gorm:"not null"`
}

// AfterMigrate is part of migrate.Migratable interface. Adds
// cascading foreign key constraints for AuthorizeData on users
// and clients.
func (a *AuthorizeData) AfterMigrate(db *gorm.DB) error {
	logger := appkitlog.Default()

	ctx, ok := appdb.GetContext(db)
	if ok {
		logger = appkitlog.ForceContext(ctx)
	}

	l := logger.With(
		"origin", "gorm",
		"context", "users.models.AuthorizeData.AfterMigrate",
	)

	l.Info().Log("msg", "Adding foreign key constraints for authorize_data...")

	if err := appdb.EnsureForeignKey(db, a, "authorize_datas", "user_id", "users", "CASCADE", "CASCADE"); err != nil {
		l.Error().Log(
			"msg", "adding foreign key user_id failed",
			"error", err.Error(),
		)
		return err
	}

	if err := appdb.EnsureForeignKey(db, a, "authorize_data", "client_id", "clients", "CASCADE", "CASCADE"); err != nil {
		l.Error().Log(
			"msg", "adding foreign key client_id failed",
			"error", err.Error(),
		)
		return err
	}

	return nil
}
