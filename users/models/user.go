package models

import (
	"errors"
	"time"

	"github.com/jinzhu/gorm"
	"golang.org/x/crypto/bcrypt"
)

// Models should only be concerned with database schema, more strict checking should be put in validator.
//
// More detail you can find here: http://jinzhu.me/gorm/models.html#model-definition
//
// HINT: If you want to split null and "", you should use *string instead of string.
type User struct {
	gorm.Model

	// basic account info
	Username string `gorm:"unique_index;not null;"`
	Role     string `gorm:"not null;"`

	// Password is a virtual attribute for updating EncryptedPassword field.
	EncryptedPassword string `gorm:"not null" json:"-"`
	Password          string `sql:"-" json:"-"`

	// other profile
	Email        string
	HeadPortrait string
	Remark       string
	NickName     string

	// SMS related
	Mobile                       string
	SmsVerificationCodeSentAt    *time.Time `json:"-"`
	SmsVerificationCode          string     `json:"-"`
	SmsVerificationCodeExpiresAt *time.Time `json:"-"`

	// WX profile
	UnionID string `gorm:"column:unionid" ` //设置json为unionid,为了跟微信的字段名保持一致！微信用户唯一标识；绑定过就不为空。

	//
	ClientUserID uint

	//
	Groups []*Group `gorm:"many2many:user_groups;"`

	// presence
	Presence int
}

const (
	ROLE_ADMIN    = "admin"
	ROLE_CUSTOMER = "customer"
	ROLE_CLIENT   = "client"

	PRESENCE_OFFLINE = 0
	PRESENCE_ONLINE  = 1
)

// What's bcrypt? https://en.wikipedia.org/wiki/Bcrypt
// Golang bcrypt doc: https://godoc.org/golang.org/x/crypto/bcrypt
// You can change the value in bcrypt.DefaultCost to adjust the security index.
// 	err := User.setPassword("password0")
func (u *User) SetPassword(password string) error {
	if len(password) == 0 {
		return errors.New("password should not be empty!")
	}

	bytePassword := []byte(password)
	// Make sure the second param `bcrypt generator cost` between [4, 32)
	passwordHash, err := bcrypt.GenerateFromPassword(bytePassword, bcrypt.DefaultCost)
	if err != nil {
		return err
	}

	u.EncryptedPassword = string(passwordHash)
	return nil
}

// Database will only save the hashed string, you should check it by util function.
// 	if err := serModel.checkPassword("password0"); err != nil { password error }
func (u *User) CheckPassword(password string) error {
	bytePassword := []byte(password)
	byteHashedPassword := []byte(u.EncryptedPassword)

	return bcrypt.CompareHashAndPassword(byteHashedPassword, bytePassword)
}

// You could update properties of an User to database returning with error info.
//  err := db.Model(User).Update(User{Username: "wangzitian0"}).Error
func (model *User) Update(db *gorm.DB, data interface{}) error {
	err := db.Model(model).Update(data).Error
	return err
}

// ErrInvalidMobileOrPassword returns an error. It's using in
// models.UserAuthenticate function when authenticate failure.
var ErrUserHadRegisterBefore = errors.New("该手机号码已经注册")
var ErrInvalidUsernameOrPassword = errors.New("用户名或密码错误,请重试")
var ErrInvalidMobileOrPassword = errors.New("手机号或密码错误,请重试")
var ErrAccountPendingApproval = errors.New("account pending approval")

// UserAuthenticate receives a mobile and a password. Then find the user
// with mobile in database and validate the password. And returns a user
// instance and an error.
func UserAuthenticate(db *gorm.DB, username, password string) (User, error) {
	var user User

	//weChat login
	if err := db.Where("unionid = ?", username).First(&user).Error; err == nil {
		return user, err
	}

	//mobile login
	if err := db.Where("mobile = ?", username).First(&user).Error; err != nil {
		if err == gorm.ErrRecordNotFound {
			return user, ErrInvalidMobileOrPassword
		}
		return user, err
	}
	// compare verification code firstly; if matches, return directly.
	now := time.Now()
	if user.SmsVerificationCodeExpiresAt != nil {
		duration := user.SmsVerificationCodeExpiresAt.Sub(now)
		if password == user.SmsVerificationCode && duration.Seconds() > 0 {
			return user, nil
		}
	}

	// compare password
	if err := bcrypt.CompareHashAndPassword([]byte(user.EncryptedPassword), []byte(password)); err != nil {
		return user, ErrInvalidMobileOrPassword
	}

	return user, nil
}
