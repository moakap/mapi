// Package oauth provides the implementations for an OAuth service.
package models

import (
	"time"

	"github.com/jinzhu/gorm"

	appkitlog "github.com/theplant/appkit/log"
	appdb "gitlab.com/moakap/mapi/db"
)

// AccessData allows API Clients to authenticate against OAuth2 API
type AccessData struct {
	gorm.Model

	User   User
	UserID uint `gorm:"index;not null"`

	Client   Client
	ClientID uint `gorm:"index;not null"`

	AccessToken  string `gorm:"not null;size:383"`
	RefreshToken string `gorm:"not null;size:255"`
	ExpiresAt    time.Time
	Scope        string `gorm:"not null"`
}

// AfterMigrate is part of migrate.Migratable interface. Adds
// cascading foreign key constraints for AccessData on users and
// clients.
func (a *AccessData) AfterMigrate(db *gorm.DB) error {
	logger := appkitlog.Default()

	ctx, ok := appdb.GetContext(db)
	if ok {
		logger = appkitlog.ForceContext(ctx)
	}

	l := logger.With(
		"origin", "gorm",
		"context", "users.models.AccessData.AfterMigrate",
	)
	l.Info().Log("msg", "Adding foreign key constraints for access tokens...")

	if err := appdb.EnsureForeignKey(db, a, "access_tokens", "user_id", "users", "CASCADE", "CASCADE"); err != nil {
		l.Error().Log(
			"msg", "adding foreign key user_id failed",
			"error", err.Error(),
		)
		return err
	}

	if err := appdb.EnsureForeignKey(db, a, "access_tokens", "client_id", "clients", "CASCADE", "CASCADE"); err != nil {
		l.Error().Log(
			"msg", "adding foreign key client_id failed",
			"error", err.Error(),
		)
		return err
	}

	return nil
}
