package models

import (
	"github.com/jinzhu/gorm"
	appkitlog "github.com/theplant/appkit/log"
	appdb "gitlab.com/moakap/mapi/db"
)

type Address struct {
	gorm.Model

	Province string `gorm:"not null"` //省
	City     string `gorm:"not null"` //市
	District string `gorm:"not null"` //区
	Details  string `gorm:"not null"` //地址详细

	// 经纬度
	Longitude float64 //经度
	Latitude  float64 //纬度

	// 其它字段
	Name    string //别名,可选
	ZipCode string //邮政编码
	Phone   string //联系电话

	// 用户
	UserID    uint `gorm:"not null"` //用户
	IsDefault bool //是否为默认
}

func (a *Address) AfterMigrate(db *gorm.DB) error {
	logger := appkitlog.Default()

	ctx, ok := appdb.GetContext(db)
	if ok {
		logger = appkitlog.ForceContext(ctx)
	}

	l := logger.With(
		"origin", "gorm",
		"context", "users.models.Address.AfterMigrate",
	)
	l.Info().Log("msg", "Adding foreign key constraints for addresses...")

	if err := appdb.EnsureForeignKey(db, a, "addresses", "user_id", "users", "CASCADE", "CASCADE"); err != nil {
		l.Error().Log(
			"msg", "adding foreign key user_id failed",
			"error", err.Error(),
		)
		return err
	}

	return nil
}
