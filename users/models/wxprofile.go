package models

import "github.com/jinzhu/gorm"

// WXProfile include wechat profile of a User.
type WXProfile struct {
	gorm.Model

	// unionID 为一个用户针对一个企业的唯一标识。
	UnionID string `gorm:"column:unionid;not null;"`

	// public
	OpenID     string `gorm:"column:openid;not null;unique_index"`
	NickName   string `gorm:"column:nickname"`
	Sex        float64
	City       string
	Country    string
	Province   string
	Language   string
	HeadImgUrl string `gorm:"column:headimgurl"`
}
