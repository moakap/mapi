package models

import (
	"github.com/jinzhu/gorm"
	appkitlog "github.com/theplant/appkit/log"
	appdb "gitlab.com/moakap/mapi/db"
)

type AuditLog struct {
	gorm.Model

	IPAddress string `gorm:"not null"`
	Action    string `gorm:"not null"`
	Details   string

	User   User
	UserID uint `gorm:"not null"`
}

func (a *AuditLog) AfterMigrate(db *gorm.DB) error {
	logger := appkitlog.Default()

	ctx, ok := appdb.GetContext(db)
	if ok {
		logger = appkitlog.ForceContext(ctx)
	}

	l := logger.With(
		"origin", "gorm",
		"context", "users.models.AuditLog.AfterMigrate",
	)
	l.Info().Log("msg", "Adding foreign key constraints for audit_logs...")

	if err := appdb.EnsureForeignKey(db, a, "audit_logs", "user_id", "users", "CASCADE", "CASCADE"); err != nil {
		l.Error().Log(
			"msg", "adding foreign key user_id failed",
			"error", err.Error(),
		)
		return err
	}

	return nil
}
