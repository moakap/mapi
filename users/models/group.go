package models

import (
	"github.com/jinzhu/gorm"
)

const (
	GROUP_TYPE = "WX"

	GROUP_EVENT_ATTACH  = "attach"
	GROUP_EVENT_DETATCH = "detatch"
)

// 分享群组
type Group struct {
	gorm.Model

	// 基本信息
	Gid  string `gorm:"unique_index" binding:"required"` // GroupID
	Name string // 组名
	Type string // 类型，默认为微信群组

	Description string // 描述

	//
	CreatedBy uint // 创建者（默认为第一个打开群分享的人）

	//
	Members []*User `gorm:"many2many:user_groups;"`
}

type GroupEventLog struct {
	gorm.Model

	//
	Event   string `gorm:"not null;" binding:"required"` // GroupID
	Gid     string `gorm:"not null;" binding:"required"` // GroupID
	Comment string

	UserID  uint
	GroupID uint

	RequestedByUserID uint
}
