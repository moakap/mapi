package models

import (
	"errors"
	"fmt"
	"strconv"
	"time"

	"gitlab.com/moakap/mapi/appconfig"

	randomdata "github.com/Pallinder/go-randomdata"

	"github.com/jinzhu/gorm"
	"gitlab.com/moakap/mapi/utils/yunsms"
)

// Sms is used to record all successfully sent SMS.
type Sms struct {
	gorm.Model

	Mobile  string `gorm:"not null;"`
	Content string `gorm:"not null;"`
	Status  string `gorm:"not null;"`

	//
	SentAt                    time.Time
	VerificationCode          string
	VerificationCodeExpiresAt time.Time
}

// createUserWithMobile is used to create new customer account with mobile.
func CreateUserWithMobile(db *gorm.DB, mobile string, clientUserID uint) (User, error) {
	user := User{
		Mobile:       mobile,
		Username:     mobile,                 // 使用手机号码作为用户名
		Password:     randomdata.SillyName(), // 随机密码
		Role:         ROLE_CUSTOMER,
		ClientUserID: clientUserID,
	}

	// Set real password
	user.SetPassword(user.Password)

	// create in DB
	err := db.Create(&user).Error
	if err != nil {
		return user, err
	}

	return user, err
}

//
func RequestVerificationCode(appconfig *appconfig.AppConfig, db *gorm.DB, mobile string) (verificationCode string, err error) {
	// 1. check whether mobile exist in DB
	user := User{}
	if err := db.Where("mobile = ?", mobile).First(&user).Error; err != nil {
		// 1.1 if not exists, create new user using the mobile.
		if err == gorm.ErrRecordNotFound {
			user, err = CreateUserWithMobile(db, mobile, 0)
			if err != nil {
				return "", err
			}
		} else {
			return "", err
		}
	}

	// 1.2 check whether existing code is still valid.
	now := time.Now()
	expiresAt := now.Add(time.Duration(300) * time.Second)
	if user.SmsVerificationCodeSentAt != nil {
		duration := now.Sub(*user.SmsVerificationCodeSentAt)
		if duration.Seconds() < 60 {
			return "", errors.New("too frequent request, try again later.")
		}
	}

	// 2. Send verification code to the mobile
	// 2.1 generate radom code
	verificationCodeNum := randomdata.Number(1000, 9999)
	// 2.2 update user verification code and expires info.
	user.SmsVerificationCode = strconv.Itoa(verificationCodeNum)
	user.SmsVerificationCodeExpiresAt = &expiresAt
	user.SmsVerificationCodeSentAt = &now
	// 2.3 send random code to mobile.
	smsContent := fmt.Sprintf("【%s】您的验证码是%s，请在%d分钟内使用验证码登录系统。",
		"noebook",
		user.SmsVerificationCode,
		5,
	)
	if err := yunsms.Send(appconfig.SMS, mobile, smsContent); err != nil {
		return "", err
	}
	// Save SMS to DB
	sms := Sms{
		Mobile:                    mobile,
		Content:                   smsContent,
		Status:                    "OK",
		SentAt:                    now,
		VerificationCode:          user.SmsVerificationCode,
		VerificationCodeExpiresAt: *user.SmsVerificationCodeExpiresAt,
	}
	// only for backup purpose, do NOT abort the error if saving to DB fails.
	db.Create(&sms)

	// update verification code to user info.
	return user.SmsVerificationCode, db.Save(&user).Error

}
