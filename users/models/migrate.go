package models

import (
	"github.com/jinzhu/gorm"
)

// Migrate the schema of database if needed
func AutoMigrate(db *gorm.DB) {
	db.AutoMigrate(&User{})
	db.AutoMigrate(&Group{})
	db.AutoMigrate(&GroupEventLog{})
	db.AutoMigrate(&Client{})
	db.AutoMigrate(&AccessData{})
	db.AutoMigrate(&AuthorizeData{})
	db.AutoMigrate(&Sms{})
	db.AutoMigrate(&WXProfile{})
	db.AutoMigrate(&Address{})
	db.AutoMigrate(&AuditLog{})
}
