package serializers

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/moakap/mapi/users/models"
)

type ProfileSerializer struct {
	C         *gin.Context
	User      *models.User
	WxProfile *models.WXProfile
}

type UserResponse struct {
	User      UserProfileResponse
	WxProfile WxProfileResponse
}

// Declare your response schema here
type UserProfileResponse struct {
	ID           uint
	Mobile       string
	Username     string
	Role         string
	Email        string
	HeadPortrait string
	Remark       string

	//
	UnionID string
}

type WxProfileResponse struct {
	ID         uint
	OpenID     string
	NickName   string
	Sex        float64
	Language   string
	City       string
	Province   string
	Country    string
	HeadImgUrl string
	UnionID    string
}

// Put your response logic including wrap the userModel here.
func (self *ProfileSerializer) UserResponse() UserProfileResponse {
	if self.User == nil {
		return UserProfileResponse{}
	}

	profile := UserProfileResponse{
		ID:           self.User.ID,
		Mobile:       self.User.Mobile,
		Username:     self.User.Username,
		Role:         self.User.Role,
		Email:        self.User.Email,
		HeadPortrait: self.User.HeadPortrait,
		Remark:       self.User.Remark,
	}

	return profile
}

func (self *ProfileSerializer) WxResponse() WxProfileResponse {
	if self.WxProfile == nil {
		return WxProfileResponse{}
	}

	profile := WxProfileResponse{
		ID:         self.WxProfile.ID,
		OpenID:     self.WxProfile.OpenID,
		NickName:   self.WxProfile.NickName,
		Sex:        self.WxProfile.Sex,
		City:       self.WxProfile.City,
		Province:   self.WxProfile.Province,
		Country:    self.WxProfile.Country,
		HeadImgUrl: self.WxProfile.HeadImgUrl,
		UnionID:    self.WxProfile.UnionID,
	}
	return profile
}

func (self *ProfileSerializer) Response() UserResponse {
	profile := UserResponse{
		User:      self.UserResponse(),
		WxProfile: self.WxResponse(),
	}
	return profile
}
