package serializers

import (
	gin "github.com/gin-gonic/gin"
)

type RegistrationSerializer struct {
	C *gin.Context

	AccessToken  string
	RefreshToken string
}

type RegistrationResponse struct {
	Profile UserResponse

	AccessToken  string `json:"access_token,omitempty"`
	RefreshToken string `json:"refresh_token,omitempty"`
}

// Put your response logic including wrap the userModel here.
func (self *RegistrationSerializer) Response() RegistrationResponse {
	profileSerializer := ProfileSerializer{
		C: self.C,
	}

	registrationResponse := RegistrationResponse{
		Profile:      profileSerializer.Response(),
		AccessToken:  self.AccessToken,
		RefreshToken: self.RefreshToken,
	}

	return registrationResponse
}
