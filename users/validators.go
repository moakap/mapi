package users

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/moakap/mapi/common"
	"gitlab.com/moakap/mapi/users/models"
)

// *ModelValidator containing two parts:
// - Validator: write the form/json checking rule according to the doc https://github.com/go-playground/validator
// - DataModel: fill with data from Validator after invoking common.Bind(c, self)
// Then, you can just call model.save() after the data is ready in DataModel.
type UserModelValidator struct {
	User struct {
		Username string `form:"username" json:"username" binding:"exists,alphanum,min=4,max=255"`
		Mobile   string `form:"mobile" json:"mobile" binding:"exists,min=11,max=11"`
		Password string `form:"password" json:"password" binding:"exists,min=8,max=255"`
		Role     string `form:"role" json:"role" binding:"exists"`
		Email    string `form:"email" json:"email"`
	} //`json:"user"`
	UserModel models.User `json:"-"`
}

// There are some difference when you create or update a model, you need to fill the DataModel before
// update so that you can use your origin data to cheat the validator.
// BTW, you can put your general binding logic here such as setting password.
func (self *UserModelValidator) Bind(c *gin.Context) error {
	err := common.Bind(c, self)
	if err != nil {
		return err
	}
	self.UserModel.Username = self.User.Username
	self.UserModel.Email = self.User.Email
	self.UserModel.Mobile = self.User.Mobile
	self.UserModel.Role = self.User.Role
	self.UserModel.SetPassword(self.User.Password)

	return nil
}

// You can put the default value of a Validator here
func NewUserModelValidator() UserModelValidator {
	userModelValidator := UserModelValidator{}
	//userModelValidator.User.Email ="w@g.cn"
	return userModelValidator
}

func NewUserModelValidatorFillWith(userModel models.User) UserModelValidator {
	userModelValidator := NewUserModelValidator()
	userModelValidator.User.Username = userModel.Username
	userModelValidator.User.Email = userModel.Email
	userModelValidator.User.Role = userModel.Role
	userModelValidator.User.Mobile = userModel.Mobile

	return userModelValidator
}
