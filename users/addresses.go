package users

import (
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
	"github.com/jinzhu/gorm"
	"github.com/theplant/appkit/log"
	"gitlab.com/moakap/mapi/common"
	"gitlab.com/moakap/mapi/db"
	"gitlab.com/moakap/mapi/users/models"
	"gitlab.com/moakap/mapi/users/oauthprovider"
)

func GetAddresses(c *gin.Context) {
	resultData := common.ResultData{}
	resultData.Code = http.StatusOK
	defer func() {
		c.JSON(resultData.Code, resultData)
	}()
	logger := log.ForceContext(c.Request.Context()).With("context", "addresses.GetAddresses")

	userID, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		resultData.Code = http.StatusUnprocessableEntity
		resultData.InternelError = err.Error()
		resultData.ErrorMsg = "userID获取异常"
		logger.Error().Log("Error:", err.Error())
		return
	}
	addresses := []models.Address{}
	gormDB := db.MustGetGorm(c)
	if err = gormDB.Where("user_id=?", userID).Where("is_delete = ?", false).Find(&addresses).Error; err != nil {
		resultData.Code = http.StatusInternalServerError
		resultData.InternelError = err.Error()
		resultData.ErrorMsg = "获取地址列表失败"
		logger.Error().Log("Error:", err.Error())
		return
	}
	resultData.Data = addresses
	return
}

func GetAddress(c *gin.Context) {
	resultData := common.ResultData{}
	resultData.Code = http.StatusOK
	defer func() {
		c.JSON(resultData.Code, resultData)
	}()
	logger := log.ForceContext(c.Request.Context()).With("context", "addresses.GetAddress")

	addressID, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		resultData.Code = http.StatusUnprocessableEntity
		resultData.InternelError = err.Error()
		resultData.ErrorMsg = "addressID获取异常"
		logger.Error().Log("Error:", err.Error())
		return
	}
	address := models.Address{}
	gormDB := db.MustGetGorm(c)
	if err = gormDB.Where("id=?", addressID).Where("is_delete = ?", false).First(&address).Error; err != nil {
		resultData.Code = http.StatusInternalServerError
		resultData.InternelError = err.Error()
		resultData.ErrorMsg = "获取地址列表失败"
		logger.Error().Log("Error:", err.Error())
		return
	}

	resultData.Data = address
	return
}

func PostAddress(c *gin.Context) {
	resultData := common.ResultData{}
	resultData.Code = http.StatusOK
	defer func() {
		c.JSON(resultData.Code, resultData)
	}()
	logger := log.ForceContext(c.Request.Context()).With("context", "addresses.PostAddress")

	address := models.Address{}
	err := c.Bind(&address)
	if err != nil {
		resultData.Code = http.StatusUnprocessableEntity
		resultData.InternelError = err.Error()
		resultData.ErrorMsg = "参数解析失败"
		logger.Error().Log("Error:", err.Error())
		return
	}

	requester, err := oauthprovider.GetRequesterFromContext(c)
	if err != nil {
		resultData.Code = http.StatusUnprocessableEntity
		resultData.InternelError = err.Error()
		resultData.ErrorMsg = "ID解析错误"
		logger.Error().Log("Error:", err.Error())
		return
	}

	address.UserID = requester.ID
	address.IsDefault = false

	gormDB := db.MustGetGorm(c)
	addressOld := models.Address{}
	if err = gormDB.Where(
		"user_id = ?",
		address.UserID,
	).Where(
		"is_delete = ?",
		false,
	).First(&addressOld).Error; err != nil && err != gorm.ErrRecordNotFound {
		resultData.Code = http.StatusInternalServerError
		resultData.InternelError = err.Error()
		resultData.ErrorMsg = "查询用户信息失败"
		logger.Error().Log("Error:", err.Error())
		return
	} else if err == gorm.ErrRecordNotFound {
		address.IsDefault = true
	}

	if err = gormDB.Create(&address).Error; err != nil {
		resultData.Code = http.StatusInternalServerError
		resultData.InternelError = err.Error()
		resultData.ErrorMsg = "添加地址失败"
		logger.Error().Log("Error:", err.Error())
		return
	}

	resultData.Data = address
	return
}

func PatchAddress(c *gin.Context) {
	resultData := common.ResultData{}
	resultData.Code = http.StatusOK
	defer func() {
		c.JSON(resultData.Code, resultData)
	}()
	logger := log.ForceContext(c.Request.Context()).With("context", "addresses.PatchAddress")

	addressID, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		resultData.Code = http.StatusUnprocessableEntity
		resultData.InternelError = err.Error()
		resultData.ErrorMsg = "地址ID获取异常"
		logger.Error().Log("Error:", err.Error())
		return
	}
	address := models.Address{}
	err = c.Bind(&address)
	if err != nil {
		resultData.Code = http.StatusUnprocessableEntity
		resultData.InternelError = err.Error()
		resultData.ErrorMsg = "参数解析失败"
		logger.Error().Log("Error:", err.Error())
		return
	}
	address.ID = uint(addressID)

	requester, err := oauthprovider.GetRequesterFromContext(c)
	if err != nil {
		resultData.Code = http.StatusUnprocessableEntity
		resultData.InternelError = err.Error()
		resultData.ErrorMsg = "ID解析错误"
		logger.Error().Log("Error:", err.Error())
		return
	}
	address.UserID = requester.ID

	gormDB := db.MustGetGorm(c)
	if address.IsDefault {
		defaultAddress := models.Address{}
		if err = gormDB.Where("user_id=?", address.UserID).Where("is_default = ?", true).Where("is_delete = ?", false).First(&defaultAddress).Error; err != nil {
			resultData.Code = http.StatusInternalServerError
			resultData.InternelError = err.Error()
			resultData.ErrorMsg = "更新默认地址信息失败"
			logger.Error().Log("Error:", err.Error())
			return
		}
		if err = gormDB.Model(&defaultAddress).Update("is_default", false).Error; err != nil {
			resultData.Code = http.StatusInternalServerError
			resultData.InternelError = err.Error()
			resultData.ErrorMsg = "更新默认地址信息失败"
			logger.Error().Log("Error:", err.Error())
			return
		}
	}

	if err = gormDB.Model(&address).Updates(&address).Error; err != nil {
		resultData.Code = http.StatusInternalServerError
		resultData.InternelError = err.Error()
		resultData.ErrorMsg = "更新地址信息失败"
		logger.Error().Log("Error:", err.Error())
		return
	}

	if err = gormDB.First(&address).Error; err != nil {
		logger.Error().Log("error:", err.Error(), ";msg:", "查询修改后的信息失败")
	}

	resultData.Data = address
	return
}

func DeleteAddress(c *gin.Context) {
	resultData := common.ResultData{}
	resultData.Code = http.StatusOK
	defer func() {
		c.JSON(resultData.Code, resultData)
	}()
	logger := log.ForceContext(c.Request.Context()).With("context", "addresses.DeleteAddress")

	addressID, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		resultData.Code = http.StatusUnprocessableEntity
		resultData.InternelError = err.Error()
		resultData.ErrorMsg = "地址ID获取异常"
		logger.Error().Log("Error:", err.Error())
		return
	}

	address := models.Address{}
	address.ID = uint(addressID)
	gormDB := db.MustGetGorm(c)
	if err = gormDB.First(&address).Error; err != nil {
		resultData.Code = http.StatusInternalServerError
		resultData.InternelError = err.Error()
		resultData.ErrorMsg = "查询要删除的地址信息失败"
		logger.Error().Log("Error:", err.Error())
		return
	}
	if err = gormDB.Model(&address).Update("is_delete", true).Error; err != nil {
		resultData.Code = http.StatusInternalServerError
		resultData.InternelError = err.Error()
		resultData.ErrorMsg = "删除地址失败"
		logger.Error().Log("Error:", err.Error())
		return
	}
	newAddress := models.Address{}
	if address.IsDefault {
		err = gormDB.Where(
			"user_id = ?",
			address.UserID,
		).Where(
			"is_delete = ?",
			false,
		).Order(
			"created_at DESC",
		).First(&newAddress).Error

		if err != nil && err != gorm.ErrRecordNotFound {
			resultData.Code = http.StatusInternalServerError
			resultData.InternelError = err.Error()
			resultData.ErrorMsg = "更新默认地址失败"
			logger.Error().Log("Error:", err.Error())
			return
		}
		if err == gorm.ErrRecordNotFound {
			resultData.Data = gin.H{"status": "OK"}
			return
		}

		newAddress.IsDefault = true
		if err = gormDB.Model(&newAddress).Updates(&newAddress).Error; err != nil {
			resultData.Code = http.StatusInternalServerError
			resultData.InternelError = err.Error()
			resultData.ErrorMsg = "更新默认地址失败"
			logger.Error().Log("Error:", err.Error())
			return
		}

	}
	resultData.Data = gin.H{"status": "OK"}
	return
}
