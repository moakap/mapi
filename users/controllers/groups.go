package controllers

import (
	"github.com/jinzhu/gorm"
	"github.com/pkg/errors"

	"gitlab.com/moakap/mapi/common"
	um "gitlab.com/moakap/mapi/users/models"
)

func QueryGroups(db *gorm.DB, scope func(db *gorm.DB) *gorm.DB) ([]um.Group, error) {
	groups := []um.Group{}

	if err := scope(db).Order("updated_at DESC").Find(&groups).Error; err != nil {
		return groups, errors.Wrap(err, "error query groups...")
	}

	return groups, nil
}

type GroupsCollection struct {
	Groups []um.Group
	Users  []um.User

	// Books        []bm.Book
	// BookProfiles []bm.BookProfile
	// Users        []um.User
}

type PagedGroups struct {
	common.Page
	GroupsCollection
}

func QueryGroupsCollection(db *gorm.DB, scope func(db *gorm.DB) *gorm.DB) (GroupsCollection, error) {
	var err error

	groups, err := QueryGroups(db, scope)
	if err != nil {
		return GroupsCollection{}, errors.Wrap(err, "error query groups")
	}

	// query other attached resources
	users := []um.User{}

	// TODO: get admins, members??
	// wg := &sync.WaitGroup{}
	// appdb.QueryInParallel(wg, db, func(db *gorm.DB) error { return queryBookProfiles(db, &books, &profiles) })
	// appdb.QueryInParallel(wg, db, func(db *gorm.DB) error { return queryBookUsers(db, &books, &users) })
	// wg.Wait()

	return GroupsCollection{
		Groups: groups,
		Users:  users,
		// BookProfiles: profiles,
	}, nil
}
