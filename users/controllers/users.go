package controllers

import (
	"github.com/jinzhu/gorm"
	"github.com/pkg/errors"

	"gitlab.com/moakap/mapi/common"
	um "gitlab.com/moakap/mapi/users/models"
)

type PagedUsers struct {
	common.Page
	Users      []um.User
	WXProfiles []um.WXProfile
}

func QueryUsers(db *gorm.DB, scope func(db *gorm.DB) *gorm.DB) ([]um.User, error) {
	users := []um.User{}

	if err := scope(db).Order("updated_at DESC").Find(&users).Error; err != nil {
		return users, errors.Wrap(err, "error query users...")
	}

	return users, nil
}

func QueryUsersCount(db *gorm.DB, scope func(db *gorm.DB) *gorm.DB) (int, error) {
	count := 0
	if err := scope(db).Model(&um.User{}).Count(&count).Error; err != nil {
		return count, errors.Wrap(err, "error query users count...")
	}

	return count, nil
}

func QueryWXProfilesByUsers(db *gorm.DB, users []um.User) ([]um.WXProfile, error) {

	unionIDList := []string{}

	for _, user := range users {
		unionIDList = append(unionIDList, user.UnionID)
	}

	profiles := []um.WXProfile{}
	if err := db.Where("unionid IN (?)", unionIDList).Find(&profiles).Error; err != nil {
		return profiles, err
	}

	return profiles, nil
}
