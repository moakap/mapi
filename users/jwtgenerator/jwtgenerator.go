// Package jwtgenerator provides a convenient way to generate JWTs
// for access/refresh token that using the *ES512* algorithm.
package jwtgenerator

import (
	"crypto/ecdsa"
	"encoding/base64"
	"fmt"
	"strings"
	"time"

	jwt "github.com/dgrijalva/jwt-go"
	"github.com/pborman/uuid"
)

// Config is configuration struct for making JWT generators via `New`
type Config struct {
	// PrivateKey is an *ECDSA* private key in PEM format for JWT
	// signing, which is generated using the *secp521r1* ECC curve.
	PrivateKey string `required:"true"`
	PublicKey  string `required:"true"`
}

// Generator defines interface for generating JWT tokens
type Generator interface {
	GenerateAccessToken(clientID string, userID uint, issuedAt, expiresAt time.Time, scopes ...string) (token string, err error)
	GenerateRefreshToken() string
}

// QClaims is a customed claims, used to generate JWT token.
type QClaims struct {
	jwt.StandardClaims
	Scopes []string `json:"scopes,omitempty"`
}

// generator is one implementation of Generator interface
type generator struct {
	privateKey *ecdsa.PrivateKey
	apiRoot    string
}

// GenerateAccessToken generates a JWT that is used for access token.
func (g *generator) GenerateAccessToken(clientID string, userID uint, issuedAt, expiresAt time.Time, scopes ...string) (token string, err error) {
	// set scopes
	nonBlankScopes := []string{}
	for _, scope := range scopes {
		if strings.TrimSpace(scope) != "" {
			nonBlankScopes = append(nonBlankScopes, scope)
		}
	}

	// create QClaims based on inputs
	claims := &QClaims{
		StandardClaims: jwt.StandardClaims{
			Issuer:    g.apiRoot,
			ExpiresAt: expiresAt.Unix(),
			Subject:   fmt.Sprintf("%d", userID),
			Audience:  clientID,
			IssuedAt:  issuedAt.Unix(),
		},
		Scopes: nonBlankScopes,
	}

	// generate JWT token
	jwtToken := jwt.NewWithClaims(jwt.SigningMethodES512, claims)
	token, err = jwtToken.SignedString(g.privateKey)

	return
}

// GenerateRefreshToken generates a JWT that is used for refresh token.
func (*generator) GenerateRefreshToken() string {
	return base64.RawURLEncoding.EncodeToString(uuid.NewRandom())
}

// New creates a new JWT generator using configuration from
// `config`. Generated tokens will have `iss` set to `apiRoot`.
func New(config Config, apiRoot string) (Generator, error) {
	g := generator{
		apiRoot: apiRoot,
	}

	privateKey, err := jwt.ParseECPrivateKeyFromPEM([]byte(config.PrivateKey))
	if err != nil {
		return nil, err
	}

	g.privateKey = privateKey

	return &g, nil
}
