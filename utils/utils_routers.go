package utils

import (
	"fmt"
	"net/http"

	"gitlab.com/moakap/mapi/appconfig"
	"gitlab.com/moakap/mapi/users/oauthprovider"
	"gitlab.com/moakap/mapi/utils/yunsms"

	"github.com/gin-gonic/gin"

	appkitlog "github.com/theplant/appkit/log"
)

func UtilsRegister(c *appconfig.AppConfig, router *gin.RouterGroup) {
	fmt.Println("==== enter UtilsRegister....")

	//SMS
	smsGroup := router.Group("/sms")
	{
		smsGroup.POST("/send", routeSMSSendHandler(c))
		smsGroup.POST("/send_from_lc", routeSMSSendViaLCHandler(c))
	}

}

//获取SMS参数
type SMSParams struct {
	//mandatory fields
	Mobile  string `binding:"required"`
	Content string `binding:"required"`
}

type LCSMSData struct {
	//mandatory fields
	Template string      `binding:"required"`
	Data     interface{} `binding:"required"`
}

func routeSMSSendHandler(config *appconfig.AppConfig) func(c *gin.Context) {
	return func(c *gin.Context) {
		logger := appkitlog.ForceContext(c.Request.Context())
		logger = logger.With("func", "routeSMSSendHandler")

		requester, err := oauthprovider.GetRequesterFromContext(c)
		if err != nil || requester == nil {
			logger.Warn().Log("msg", "unauthorized opeation request, return 401")
			// c.JSON(http.StatusUnauthorized, gin.H{"error": oauthprovider.ErrAccessDenied})
			// return
		}

		// get SMS params
		smsParams := SMSParams{}
		if err := c.BindJSON(&smsParams); err != nil {
			logger.Error().Log("msg", "bind JSON failed",
				"error", err.Error(),
			)
			c.JSON(http.StatusUnprocessableEntity, gin.H{"error": err.Error()})
			return
		}

		if err := yunsms.Send(config.SMS, smsParams.Mobile, smsParams.Content); err != nil {
			logger.Error().Log(
				"msg", "SMS send failed",
				"error", err.Error(),
			)

			c.JSON(
				http.StatusOK,
				gin.H{
					"Status": "Failed",
					"ErrMsg": err.Error(),
				},
			)
			return
		}

		c.JSON(
			http.StatusOK,
			gin.H{
				"Status": "Success",
			},
		)
		return
	}
}

func routeSMSSendViaLCHandler(config *appconfig.AppConfig) func(c *gin.Context) {
	return func(c *gin.Context) {
		logger := appkitlog.ForceContext(c.Request.Context())
		logger = logger.With("func", "routeSMSSendViaLCHandler")

		requester, err := oauthprovider.GetRequesterFromContext(c)
		if err != nil || requester == nil {
			logger.Warn().Log("msg", "unauthorized opeation request, return 401")
			// c.JSON(http.StatusUnauthorized, gin.H{"error": oauthprovider.ErrAccessDenied})
			// return
		}

		// get SMS params
		smsParams := LCSMSData{}
		if err := c.BindJSON(&smsParams); err != nil {
			logger.Error().Log("msg", "bind JSON failed",
				"error", err.Error(),
			)
			c.JSON(http.StatusUnprocessableEntity, gin.H{"error": err.Error()})
			return
		}

		if err := yunsms.SendViaLeanCloud(config.SMS, smsParams.Template, smsParams.Data); err != nil {
			logger.Error().Log(
				"msg", "SMS send failed",
				"error", err.Error(),
			)

			c.JSON(
				http.StatusOK,
				gin.H{
					"Status": "Failed",
					"ErrMsg": err.Error(),
				},
			)
			return
		}

		c.JSON(
			http.StatusOK,
			gin.H{
				"Status": "Success",
			},
		)
		return
	}
}
