package qiniup

import (
	"context"
	"fmt"

	"github.com/qiniu/api.v7/v7/auth/qbox"
	"github.com/qiniu/api.v7/v7/storage"
)

type Config struct {
	AccessKey string
	SecretKey string
	Bucket    string
}

func QiNiuUploadToken(config *Config, fileName string) (upToken string) {
	// 覆盖上传凭证
	// 需要覆盖的文件名
	keyToOverwrite := fileName
	putPolicy := storage.PutPolicy{
		Scope: fmt.Sprintf("%s:%s", config.Bucket, keyToOverwrite),
	}

	mac := qbox.NewMac(config.AccessKey, config.SecretKey)
	upToken = putPolicy.UploadToken(mac)
	return
}

func QiNiuUploadSimpleToken(config *Config) (upToken string) {
	// 简单上传凭证
	putPolicy := storage.PutPolicy{
		Scope: config.Bucket,
	}

	mac := qbox.NewMac(config.AccessKey, config.SecretKey)
	upToken = putPolicy.UploadToken(mac)
	return
}

func QiNiuUpload(config *Config, localPath, photoName string) error {
	token := ""
	if photoName != "" {
		token = QiNiuUploadToken(config, photoName)
	} else {
		token = QiNiuUploadSimpleToken(config)
	}
	cfg := storage.Config{}
	// 空间对应的机房
	cfg.Zone = &storage.ZoneHuadong
	// 是否使用https域名
	cfg.UseHTTPS = false
	// 上传是否使用CDN上传加速
	cfg.UseCdnDomains = false
	// 构建表单上传的对象
	formUploader := storage.NewFormUploader(&cfg)
	ret := storage.PutRet{}
	putExtra := storage.PutExtra{
		Params: map[string]string{
			"x:name": "QrCode",
		},
	}
	err := formUploader.PutFile(context.Background(), &ret, token, photoName, localPath, &putExtra)
	if err != nil {
		return err
	}
	return nil
}
