package yunsms

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"strconv"
	"strings"
	"time"

	"gitlab.com/moakap/mapi/utils/encrypt"
)

const (
	DEFAULT_ENCODE = "utf-8"
	HOST           = "http://http.yunsms.cn"
	SEND_PATH      = "tx"
	RECV_PATH      = "rx"
	GET_REST_PATH  = "mm"
)

// Refer to http://yunsms.cn/api.html for detailed doc.
type YunSmsConfig struct {
	Uid string
	Pwd string

	LcAppId  string
	LcAppKey string
}

// SMS message
type Message struct {
	StatusCode string
	Mobile     string
	Content    string
	Gateway    string
	Time       time.Time
}

//getMd5String is to generte 32-bit MD5 code of the inout string.
// func getMd5String(s string) string {
// 	h := md5.New()
// 	h.Write([]byte(s))
// 	return hex.EncodeToString(h.Sum(nil))
// }

//
func doRequest(method string, url string, data interface{}) (*http.Response, error) {
	// 1. prepare the body
	jsonData, err := json.Marshal(data)
	if err != nil {
		return nil, err
	}
	fmt.Println("request body: ", string(jsonData))

	// 2. prepare the requester
	req, err := http.NewRequest(method, url, bytes.NewBuffer(jsonData))
	if err != nil {
		return nil, err
	}

	//  set headers
	req.Header.Set("Content-Type", "application/json")

	// 3. DO request.
	client := http.Client{}
	return client.Do(req)

}

func codeToStatus(code string) string {
	var status string

	switch code {
	case "100":
		status = "发送成功"
	case "101":
		status = "验证失败（账号密码错误）"
	case "102":
		status = "短信不足"
	case "103":
		status = "操作失败"
	case "104":
		status = "非法字符"
	case "105":
		status = "内容过多"
	case "106":
		status = "号码过多"
	case "107":
		status = "频率过快"
	case "108":
		status = "号码内容为空"
	case "109":
		status = "账号异常"
	case "110":
		status = "禁止频繁单条发送"
	case "111":
		status = "账号暂停发送"
	case "112":
		status = "号码错误"
	case "113":
		status = "定时时间格式不正确"
	case "114":
		status = "账号临时锁定，10分钟后自动解锁"
	case "115":
		status = "连接失败"
	case "116":
		status = "禁止接口发送"
	case "117":
		status = "绑定IP错误"
	case "120":
		status = "系统升级"
	default:
		status = "未知错误码"
	}

	return status
}

// Send will send SMS to specific mobile(s).
func Send(config YunSmsConfig, mobile string, content string) error {
	path := fmt.Sprintf("%s/%s/?uid=%s&pwd=%s&mobile=%s&content=%s&encode=utf8",
		HOST,
		SEND_PATH,
		config.Uid,
		encrypt.GetMd5String(config.Pwd),
		mobile,
		url.QueryEscape(content),
	)

	// debug purpose
	urlStr, err := url.QueryUnescape(path)
	if err == nil {
		fmt.Println("do http request to path: ", urlStr)
	}

	res, err := doRequest("POST", path, nil)
	if err != nil {
		return err
	}

	//
	defer res.Body.Close()

	// check status code
	if res.StatusCode != http.StatusOK {
		return errors.New(res.Status)
	}

	// handling response body
	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return err
	}

	// response body handling
	if string(body) != "100" {
		return errors.New(codeToStatus(string(body)))
	}

	return nil
}

// GetRestAmount is to get rest amount of the account.
func GetRestAmount(config YunSmsConfig) (int, error) {
	path := fmt.Sprintf("%s/%s/?uid=%s&pwd=%s",
		HOST,
		GET_REST_PATH,
		config.Uid,
		encrypt.GetMd5String(config.Pwd),
	)

	// debug purpose
	urlStr, err := url.QueryUnescape(path)
	if err == nil {
		fmt.Println("do http request to path: ", urlStr)
	}

	res, err := doRequest("POST", path, nil)
	if err != nil {
		return -1, err
	}
	defer res.Body.Close()

	// check status code
	if res.StatusCode != http.StatusOK {
		return -1, errors.New(res.Status)
	}

	// handling response body
	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return -1, err
	}

	// response body handling
	statusList := strings.Split(string(body), "||")
	if len(statusList) < 2 || statusList[0] != "100" {
		return -1, errors.New(codeToStatus(statusList[0]))
	}

	return strconv.Atoi(statusList[1])
}

// GetReplyMsgs is to get all replies
func GetReplyMsgs(config YunSmsConfig) ([]Message, error) {
	// msgList := []Message{}
	panic(errors.New("not implemented!!!"))
	// return msgList, nil
}

//
func doLeanCloudRequest(lcAppId string, lcAppKey string, method string, url string, data interface{}) (*http.Response, error) {
	// 1. prepare the body
	jsonData, err := json.Marshal(data)
	if err != nil {
		return nil, err
	}
	fmt.Println("request body: ", string(jsonData))

	// 2. prepare the requester
	req, err := http.NewRequest(method, url, bytes.NewBuffer(jsonData))
	if err != nil {
		return nil, err
	}

	//  set headers
	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("X-LC-Id", lcAppId)
	req.Header.Set("X-LC-Key", lcAppKey)

	// 3. DO request.
	client := http.Client{}
	return client.Do(req)
}

// 尊敬的会员，您好！茵曼漯河千盛店感谢您的惠顾。您于{{{paidDate}}}通过{{{paidType}}}消费{{{paidAmount}}}元，
// 预计可获得{{{cashbackAmount}}}元消费奖励，加入店铺VIP可兑换无门槛代金券，新用户可直接获得{{{newMemberBonus}}}元奖励。
// 微信搜索“茵缘VIP”小程序加入，邀请朋友加入还可获得粉丝消费奖励，赶紧行动吧！
func SendViaLeanCloud(config YunSmsConfig, template string, data interface{}) error {
	// path := fmt.Sprintf("%s/%s/?uid=%s&pwd=%s&mobile=%s&content=%s&encode=utf8",
	// 	HOST,
	// 	SEND_PATH,
	// 	config.Uid,
	// 	encrypt.GetMd5String(config.Pwd),
	// 	mobile,
	// 	url.QueryEscape(content),
	// )

	path := "https://cy4yalks.lc-cn-n1-shared.com"
	path += "/1.1/requestSmsCode"

	// debug purpose
	urlStr, err := url.QueryUnescape(path)
	if err == nil {
		fmt.Println("do http request to path: ", urlStr)
	}

	res, err := doLeanCloudRequest(config.LcAppId, config.LcAppKey, "POST", path, data)
	if err != nil {
		return err
	}

	//
	defer res.Body.Close()

	// check status code
	if res.StatusCode != http.StatusOK {
		return errors.New(res.Status)
	}

	// // handling response body
	// body, err := ioutil.ReadAll(res.Body)
	// if err != nil {
	// 	return err
	// }

	// // response body handling
	// if string(body) != "100" {
	// 	return errors.New(codeToStatus(string(body)))
	// }

	return nil
}
