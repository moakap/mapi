# 用户相关接口

## 用户管理

### 获取短信验证码

通过手机号码获取验证码。

#### POST /oauth/verification_code

```http
POST /oauth/verification_code?mobile=138xxxxxxxx
```

#### 参数说明

| 参数   | 是否必须 | 说明     |
| ------ | -------- | -------- |
| mobile | YES      | 手机号码 |

#### 返回结果

- 正确返回

``` json
{"status":"OK"}
```



- 错误返回

```json
{

    "status":"",

    "error":"xxxx"

}
```



### 登录（获取access token）

通过手机号码+验证码/密码获取access token。

#### POST /oauth/token

```http
POST /oauth/token?username=18626881965&password=123456&grant_type=password&client_id=app&client_secret=secret
```

注意：支持的三种登录方式

1. 用户名+密码
2. 手机号码+验证码
3. 微信unionid （不要密码，直接登录，密码会被忽略）

#### 参数说明

| 参数          | 是否必须 | 说明                                |
| ------------- | -------- | ----------------------------------- |
| username      | YES      | 用户名，可以是手机号码、微信unionid |
| password      | YES      | 密码                                |
| grant_type    | YES      | password                            |
| client_id     | YES      | app                                 |
| client_secret | YES      | secret                              |

#### 返回说明

- 正确返回

```json
{
"access_token": "eyJhbGciOiJFUzUxMiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJhcHAiLCJleHAiOjE1MzU0NTUxMjYsImlhdCI6MTUzNTM2MTUyNiwiaXNzIjoiaHR0cDovL3EtdG91ci1hcGkub2JlbmJlbi5jb20vYXBpIiwic3ViIjoiMSJ9.ARgiLReB5JLoYH8_ArlZmcTXLB_MpXvY8lM9Z8t-RF3evnKH8D4rLCh92jMvk_2dmCqQ0-m5LHrxKkVT9tdNWIO4AKd_3Im4kHzfJddAHNC2D9o0F1bn-neMCBs33SJ5cWiz8CY8hOk4PKievUIYSiYuu6lcsjEhXJfu0Vhv_n-NxmP8",
"expires_in": 93600,
"refresh_token": "wFUHKCm6R-qvr0PvBJrrcw",
"token_type": "Bearer"
}
```

- 错误返回

```json
{
"error": "access_denied",
"error_description": "The resource owner or authorization server denied the request."
}
```



### 修改密码

通过用户名和验证码修改用户密码。

#### POST /oauth/set_password

```http
POST /oauth/set_password
```

#### 参数说明

- body结构体

  ```json
  {"username":"18626881965", "password":"123456", "verification_code":"1234"}
  ```

#### 返回结果

- 成功修改密码，返回用户基本信息 - 200

  ```json
  {
  "data": {
  "id": 1,
  "mobile": "18626881965",
  "username": "18626881965",
  "role": "customer",
  "email": "",
  "head_portrait": "",
  "remark": "",
  "unionid": ""
  },
  "status": "OK"
  }
  ```

- 验证码超时 - 403

  ```json
  {
  "error": "verification code expired"
  }
  ```

- 验证码错误 - 403

  ```json
  {
  "error": "incorrect verifaction code"
  }
  ```

- 用户名不存在 - 422

  ```json
  {
  "error": "username does not exist"
  }
  ```


### 获取个人信息（用户）

用户个人信息页获取个人信息。

#### GET /api/profile

```http
GET /api/profile
```



#### 参数说明

| 参数 | 是否必须(YES/NO) | 说明 |
| ---- | ---------------- | ---- |
| -    | -                | -    |



#### 返回结果

- 成功返回

  ```json
  {
  "data": {
      "user": {
          "id": 5,
          "mobile": "18626881961",
          "username": "18626881961",
          "role": "admin",
          "email": "",
          "head_portrait": "",
          "remark": "",
          "unionid": ""
      },
      "wx_profile": {
          "id": 0,
          "openid": "",
          "nickname": "",
          "sex": 0,
          "language": "",
          "city": "",
          "province": "",
          "country": "",
          "headimgurl": "",
          "unionid": ""
      }
  }
  }
  ```

  - 如果unionid不为空，则wx_profile为对应的微信用户信息；



### 获取用户信息（管理员）

管理员获取用户个人信息。

#### GET /api/users/:id  

``` http
GET /api/users/:id
```

#### 参数说明

| 参数 | 是否为必须(YES/NO) | 说明   |
| ---- | ------------------ | ------ |
| id   | YES                | 用户ID |
|      |                    |        |



#### 返回结果

- 成功结果

  ```json
  {
  "data": {
      "user": {
          "id": 2,
          "mobile": "18626881964",
          "username": "18626881964",
          "role": "customer",
          "email": "",
          "head_portrait": "",
          "remark": "",
          "unionid": ""
      },
      "wx_profile": {
          "id": 0,
          "openid": "",
          "nickname": "",
          "sex": 0,
          "language": "",
          "city": "",
          "province": "",
          "country": "",
          "headimgurl": "",
          "unionid": ""
      }
  }
  }
  ```


### 【NOK】获取用户列表（管理员）

管理员按条件筛选用户列表。

#### GET /api/users

```http
GET /api/users
```

#### 参数说明

| 参数      | 是否必须(YES/NO) | 说明                   |
| --------- | ---------------- | ---------------------- |
| mobile    | NO               | 按手机号码搜索         |
| username  | NO               | 按用户名关键字查询     |
| role      | NO               | 按角色筛选，默认不筛选 |
| order_by  | NO               | asc/desc               |
| page      | NO               | 默认值0                |
| page_size | NO               | 默认值10               |



#### 返回结果



### 修改个人信息

管理员或用户修改个人信息。

#### PUT /api/users/:id 

```http
PUT /api/users/:id
```

#### 参数说明

```json
{
    "role":"customer", // 只有admin才能修改用户角色
    "email":"test@obenben.com", 
    "head_portrait":"obenben.com/images/icon.png", 
    "remark":"this is only for testing purpose.", 
    "mobile":"18626881967"
}
```



#### 返回结果

- 成功结果

  ```json
  {
  	"data": {
          "id": 1,
          "mobile": "18626881967",
          "username": "",
          "role": "customer",
          "email": "test@obenben.com",
          "head_portrait": "obenben.com/images/icon.png",
          "remark": "this is only for testing purpose.",
          "unionid": ""
  	}
  }
  ```

### 修改个人微信信息

管理员或用户修改个人微信信息。

#### POST /api/users/:id/wx_profile

```http
POST /api/users/:id/wx_profile
```

#### 参数说明

```go
type WXProfile struct {
	gorm.Model

	// unionID 为一个用户针对一个企业的唯一标识。
	UnionID string `gorm:"column:unionid;not null;"`

	// public
	OpenID     string `gorm:"column:openid;not null;unique_index"`
	NickName   string `gorm:"column:nickname"`
	Sex        float64
	City       string
	Country    string
	Province   string
	Language   string
	HeadImgUrl string `gorm:"column:headimgurl"`
}
```



#### 返回结果

- 成功结果

  ```json
  
  ```
