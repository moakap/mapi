# 微信小程序相关接口

## 小程序相关接口

### 获取session_key

通过微信code获取session_key, openid等. 参考 [开放接口文档](https://developers.weixin.qq.com/miniprogram/dev/api/api-login.html).  

![img](https://developers.weixin.qq.com/miniprogram/dev/image/api-login.jpg?t=18082922)



#### GET /wechat/session_key



#### 参数说明

| 参数 | 是否必须(YES/NO) | 说明 |
| ---- | ---------------- | ---- |
| code | YES              |      |
|      |                  |      |



#### 返回结果



### 注册新用户

通过微信profile注册新用户

#### POST /wechat

#### 参数说明

```go
type WXProfile struct {
	gorm.Model

	// unionID 为一个用户针对一个企业的唯一标识。
	UnionID string `gorm:"column:unionid;not null;"`

	// public
	OpenID     string `gorm:"column:openid;not null;unique_index"`
	NickName   string `gorm:"column:nickname"`
	Sex        float64
	City       string
	Country    string
	Province   string
	Language   string
	HeadImgUrl string `gorm:"column:headimgurl"`
}
```



#### 返回结果

```json
{
	"data": {
        "id": 1,
        "mobile": "18626881967",
        "username": "",
        "role": "customer",
        "email": "test@obenben.com",
        "head_portrait": "obenben.com/images/icon.png",
        "remark": "this is only for testing purpose.",
        "unionid": ""
	}
}
```

