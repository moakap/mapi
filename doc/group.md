# group 相关接口

## 基本接口
### POST /groups 新建或更新group
- 参数
- body
```json
{"Gid":"wx_123456", "Name":"my family", "Type":"wx", "Description":"xxxx"}
```
- 返回结果
  - 201 Gid不存在，新建成功； 
  - 200 Gid已经存在，更新成功
  - others 错误

### GET /groups 获取所有group （管理员使用）
TODO

### GET /my_groups 获取所属的groups
- 参数
  - 无
- 返回结果
  ```json
    {
      "Data": [
        {
      "ID": 1,
      "CreatedAt": "2019-03-31T22:14:35+08:00",
      "UpdatedAt": "2019-03-31T22:14:35+08:00",
      "DeletedAt": null,
      "Gid": "wx_123456",
      "Name": "my family",
      "Type": "wx",
      "Description": "",
      "CreatedBy": 5,
      "Members": null
      },
        {
      "ID": 2,
      "CreatedAt": "2019-03-31T22:15:27+08:00",
      "UpdatedAt": "2019-03-31T22:15:27+08:00",
      "DeletedAt": null,
      "Gid": "wx_123457",
      "Name": "my family",
      "Type": "wx",
      "Description": "",
      "CreatedBy": 5,
      "Members": null
      }
      ],
      }

  ```

### POST /attach_to_group/:gid 关联到group
- 参数
  - gid OpenGid
- body
    ```json
    {"Event":"attach", "Comment":"just test attach operation", "UserID": 1}
    ```
  - event attach / detatch 
  - Comment 备注
  - UserID 默认填写当前用户ID
- 返回结果
  ```json
  {
    "Data": {
    "ID": 1,
    "CreatedAt": "2019-03-31T22:14:35+08:00",
    "UpdatedAt": "2019-03-31T22:14:35+08:00",
    "DeletedAt": null,
    "Gid": "wx_123456",
    "Name": "my family",
    "Type": "wx",
    "Description": "",
    "CreatedBy": 5,
    "Members": [
      {
    "ID": 5,
    "CreatedAt": "2018-08-25T17:02:27+08:00",
    "UpdatedAt": "2019-04-08T00:06:42+08:00",
    "DeletedAt": null,
    "Username": "18626881961",
    "Role": "admin",
    "Email": "",
    "HeadPortrait": "",
    "Remark": "",
    "Mobile": "18626881961",
    "UnionID": "",
    "ClientUserID": 0,
    "Groups": null
    }
    ],
    }
    }
  ```