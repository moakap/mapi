# friends

## friends相关接口

### 获取好友

#### GET /friends

```http
GET /friends?user_id=2&type=1&weight=3&state=accepted&page=0&page_size=20
```



#### 参数说明

| 参数            | 是否必须(YES/NO) | 说明                                 |
| --------------- | ---------------- | ------------------------------------ |
| ```type```      | NO               | 类型：0 - 不限， 单向（1）/双向（2） |
| ```weight```    | NO               | 权重                                 |
| ```user_id```   | NO               | 用户ID，用于获取某个用户的好友关系   |
| ```state```     | NO               | 好友关系状态，可选"accepted/pending" |
| ```page```      | NO               | 分页参数，默认0（第一页）            |
| ```page_size``` | NO               | 分页参数，页面大小，默认20.          |



#### 返回结果

- 成功结果

  ```json
  {
      "data": {
          "total_count": 1,
          "total_page": 1,
          "current_page": 0,
          "page_size": 10,
          "friends": [
                {
                  "ID": 10,
                  "CreatedAt": "2018-08-25T20:15:07+08:00",
                  "UpdatedAt": "2018-08-25T20:15:52+08:00",
                  "DeletedAt": null,
                  "src_user_id": 1,
                  "dest_user_id": 2,
                  "weight": 1,
                  "type": 1
              }
          ],
          "users": [
                {
                  "ID": 1,
                  "CreatedAt": "2018-08-18T11:35:10+08:00",
                  "UpdatedAt": "2018-09-01T20:19:36+08:00",
                  "DeletedAt": null,
                  "username": "18626881965",
                  "role": "customer",
                  "email": "test@obenben.com",
                  "head_portrait": "obenben.com/images/icon.png",
                  "remark": "this is only for testing purpose.",
                  "mobile": "18626881967",
                  "unionid": "",
                  "ClientUserID": 0
              },
                {
                  "ID": 2,
                  "CreatedAt": "2018-08-25T17:00:31+08:00",
                  "UpdatedAt": "2018-08-25T17:00:31+08:00",
                  "DeletedAt": null,
                  "username": "18626881964",
                  "role": "customer",
                  "email": "",
                  "head_portrait": "",
                  "remark": "",
                  "mobile": "18626881964",
                  "unionid": "",
                  "ClientUserID": 0
              }
          ],
      }
  }
  ```




### 批量上传朋友关系

####POST /batch/friends



#### 参数说明

```json
{
    "friends":
    [
        {"user_id": 2, "type":1, "weight":1}, 
        {"user_id": 3, "type":1, "weight":1}, 
        {"user_id": 5, "type":1, "weight":1}
    ]
}
```



#### 返回结果

```json
{
    "data": [
          {
        "ID": 14,
        "CreatedAt": "2018-09-02T08:16:34.984513963+08:00",
        "UpdatedAt": "2018-09-02T08:16:34.984513963+08:00",
        "DeletedAt": null,
        "src_user_id": 2,
        "dest_user_id": 5,
        "weight": 1,
        "type": 1
        },
          {
        "ID": 15,
        "CreatedAt": "2018-09-02T08:16:34.985671823+08:00",
        "UpdatedAt": "2018-09-02T08:16:34.985671823+08:00",
        "DeletedAt": null,
        "src_user_id": 3,
        "dest_user_id": 5,
        "weight": 1,
        "type": 1
        },
          {
        "ID": 16,
        "CreatedAt": "2018-09-02T08:16:34.98672627+08:00",
        "UpdatedAt": "2018-09-02T08:16:34.98672627+08:00",
        "DeletedAt": null,
        "src_user_id": 5,
        "dest_user_id": 5,
        "weight": 1,
        "type": 1
        }
    ],
}
```



### 朋友关系跳转

#### POST /friends/transition



#### 参数说明

```json
{
    "user_id": 2, 
    "note":"testing transition", 
    "event":"add_request" 
}
```



- Event说明

| Event             | 说明                          |
| ----------------- | ----------------------------- |
| ```add_request``` | 请求加好友，user_id必填       |
| ```add_accept```  | 接受还有请求，user_id必填     |
| ```add_reject```  | 拒绝添加好友请求，user_id必填 |
| ```remove```      | 删除好友，user_id必填         |
- State说明

| State          | 说明               |
| -------------- | ------------------ |
| ```new```      | 新建状态（内部）   |
| ```pending```  | 好友请求待处理状态 |
| ```accepted``` | 好友请求已接受     |
| ```rejected``` | 好友请求已拒绝     |
| ```removed```  | 好友关系已删除     |
|                |                    |



#### 返回结果

```json
{
    "data": {
        "ID": 1,
        "CreatedAt": "2018-09-02T23:55:43+08:00",
        "UpdatedAt": "2018-09-02T23:59:16.018553657+08:00",
        "DeletedAt": null,
        "state": "pending",
        "src_user_id": 2,
        "dest_user_id": 5,
        "weight": 0,
        "type": 0,
        "last_requested_by_user_id": 5,
        "last_request_note": "testing transition"
    }
}
```

