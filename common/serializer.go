// Package serializer provides a series of struct
// for easy encode/decode struct data to/from database
package common

import (
	"database/sql"
	"database/sql/driver"
	"encoding/json"
	"fmt"
	"time"
)

// JSONArray is a struct in order to serialize javascript's JSON array
// for example: [{"foo":"bar", "hello": "world"}]
type JSONArray []map[string]interface{}

// Scan is required to implement Scanner interface for database/sql/driver package
func (s *JSONArray) Scan(src interface{}) error {
	err := json.Unmarshal(src.([]byte), s)
	return err
}

// Value is required to implement Valuer interface for database/sql/driver package
func (s JSONArray) Value() (driver.Value, error) {
	return json.Marshal(s)
}

// JSONObject is a struct in order to serialize a map as a JSON object
// for example: {"foo":"bar", "hello": "world"}
type JSONObject map[string]interface{}

// Scan is part of database/sql.Scanner
func (s *JSONObject) Scan(src interface{}) error {
	err := json.Unmarshal(src.([]byte), s)
	return err
}

// Value is part of database/sql/driver.Valuer
func (s JSONObject) Value() (driver.Value, error) {
	return json.Marshal(s)
}

// ================ NullString struct

// NewNullString helper to create a NullString
func NewNullString(s *string) NullString {
	ns := NullString{}
	if s != nil {
		ns.String = *s
		ns.Valid = true
	}
	return ns
}

// NullString represents a string that may be null. NullString embed the
// sql.NullString to implement both json.Marshaler and json.Unmarshaler
// interface so it can be used as a json encode/decode destination.
type NullString struct {
	sql.NullString
}

// MarshalJSON part of json.Marshaler.
func (ns NullString) MarshalJSON() ([]byte, error) {
	if !ns.Valid {
		return json.Marshal(nil)
	}
	return json.Marshal(ns.String)
}

// UnmarshalJSON part of json.Unmarshaler
func (ns *NullString) UnmarshalJSON(data []byte) error {
	var val interface{}

	if err := json.Unmarshal(data, &val); err != nil {
		return err
	}

	if val == nil {
		return nil
	}

	str, ok := val.(string)
	if ok {
		ns.Valid = true
		ns.String = str
		return nil
	}

	return fmt.Errorf("must be a string, got %v(%T)", str, str)
}

// ================ NullTime struct

// NewNullTime helper to create a NullTime
func NewNullTime(t *time.Time) NullTime {
	nt := NullTime{}
	if t != nil {
		nt.Time = *t
		nt.Valid = true
	}
	return nt
}

// NullTime represents a time that may be null. NullTime implements
// the Scanner interface so it can be used as a scan destination,
// similar to sql.NullString.
type NullTime struct {
	time.Time
	Valid bool
}

// Scan implements the Scanner interface. It parses the date value
// (fetch from database) to time.
func (nt *NullTime) Scan(value interface{}) error {
	if value == nil {
		return nil
	}

	t, ok := value.(time.Time)
	if ok {
		nt.Time, nt.Valid = t, true
		return nil
	}

	return fmt.Errorf("can't parse %T(%v) to time.Time", value, value)
}

// Value implements the driver Valuer interface. It pass the time
// value to database. It will pass nil if there's no value.
func (nt NullTime) Value() (driver.Value, error) {
	if nt.Valid {
		return nt.Time, nil
	}
	return nil, nil
}

// ================ NullDate struct

// dateFormat defines time format for NullDate type.
const dateFormat = "2006-01-02"

// NullDate provides a way to:
//   1. Store value as `date` type in database.
//   2. Support store NULL-value in database.
//   3. Decode/encode value from/to json like: "2015-10-29" and null.
type NullDate struct {
	NullTime
}

// NewNullDate helper to create a NullDate
func NewNullDate(t *time.Time) NullDate {
	return NullDate{NullTime: NewNullTime(t)}
}

// UnmarshalJSON implements the json.Unmarshaler interface.
// It ensures NullDate value only accepts:
//   1. A date string value, like: "2015-01-01"
//   2. A "null" string value
//
// Will parse the accepted value to time.Time type and pass
// it to NullDate. Panics on other unexpected value.
func (d *NullDate) UnmarshalJSON(data []byte) error {
	var rawData interface{}
	if err := json.Unmarshal(data, &rawData); err != nil {
		return err
	}

	if rawData == nil {
		d.Valid = false
		return nil
	}

	s, ok := rawData.(string)

	if !ok {
		return fmt.Errorf("date should be a string, got %s", data)
	}

	t, err := time.Parse(dateFormat, s)

	if err != nil {
		return fmt.Errorf("invalid date: %v", err)
	}

	d.Time = t
	d.Valid = true
	return nil
}

// MarshalJSON implements the json.Marshaler interface.
// It encodes the NullDate value to a string with format
// XXXX-XX-XX.
func (d NullDate) MarshalJSON() ([]byte, error) {
	if d.Valid {
		return json.Marshal(d.String())
	}
	return json.Marshal(nil)
}

// String implements the fmt.Stringer interface.
// Will formatted the value of NullDate type to XXXX-XX-XX.
func (d NullDate) String() string {
	return d.Time.Format(dateFormat)
}

// ================ NullTimezone struct

// NullTimezone wraps sql.NullString and provides a way to:
//   1. Stores a nullable timezone in database.
//   2. Decode/encode timezone from/to json like: "Australia/Sydney" and *null*.
type NullTimezone struct {
	sql.NullString
}

// UnmarshalJSON implements the json.Unmarshaler interface.
// It ensures NullTimezone value only accepts:
//   1. A timezone string value, like: "Australia/Sydney"
//   2. A *null* string value
//
// Will panic if given value is:
//   1. no string
//   2. a empty string
//   3. an invalid timezone string
func (t *NullTimezone) UnmarshalJSON(data []byte) error {
	var d interface{}
	if err := json.Unmarshal(data, &d); err != nil {
		return err
	}

	if d == nil {
		t.Valid = false
		return nil
	}

	s, ok := d.(string)
	if !ok {
		return fmt.Errorf("timezone should be a string, got %s", s)
	}

	if s == "" {
		return fmt.Errorf("timezone can not empty, got %s", s)
	}

	_, err := time.LoadLocation(s)
	if err != nil {
		return fmt.Errorf("invalid timezone: %s", s)
	}

	t.Valid = true
	t.String = s
	return nil
}

// MarshalJSON implements the json.Marshaler interface.
// It encodes the NullTimezone value to a string or *null*.
func (t NullTimezone) MarshalJSON() ([]byte, error) {
	if !t.Valid {
		return json.Marshal(nil)
	}
	return json.Marshal(t.String)
}
