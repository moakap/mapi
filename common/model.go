package common

/*
// Model struct contains minimal columns for a model. It overwrites
// the `gorm.Model` struct with:
// - remove `DeletedAt` column to disable soft-delete
// - custom JSON struct tag for ID
type Model struct {
	ID        uint      `gorm:"primary_key" json:"id"`
	CreatedAt time.Time `sql:"not null" json:"-"`
	UpdatedAt time.Time `sql:"not null" json:"-"`
}
*/
