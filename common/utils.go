// Common tools and helper functions
package common

import (
	crand "crypto/rand"
	"encoding/base64"
	"fmt"
	"math/big"

	"gopkg.in/go-playground/validator.v8"

	"github.com/gin-gonic/gin"
	"github.com/gin-gonic/gin/binding"
)

// My own Error type that will help return my customized Error info
//  {"database": {"hello":"no such table", error: "not_exists"}}
type CommonError struct {
	Errors map[string]interface{} `json:"errors"`
}

// To handle the error returned by c.Bind in gin framework
// https://github.com/go-playground/validator/blob/v9/_examples/translations/main.go
func NewValidatorError(err error) CommonError {
	res := CommonError{}
	res.Errors = make(map[string]interface{})
	errs := err.(validator.ValidationErrors)
	for _, v := range errs {
		// can translate each error one at a time.
		//fmt.Println("gg",v.NameNamespace)
		if v.Param != "" {
			res.Errors[v.Field] = fmt.Sprintf("{%v: %v}", v.Tag, v.Param)
		} else {
			res.Errors[v.Field] = fmt.Sprintf("{key: %v}", v.Tag)
		}

	}
	return res
}

// Warp the error info in a object
func NewError(key string, err error) CommonError {
	res := CommonError{}
	res.Errors = make(map[string]interface{})
	res.Errors[key] = err.Error()
	return res
}

// Changed the c.MustBindWith() ->  c.ShouldBindWith().
// I don't want to auto return 400 when error happened.
// origin function is here: https://github.com/gin-gonic/gin/blob/master/context.go
func Bind(c *gin.Context, obj interface{}) error {
	b := binding.Default(c.Request.Method, c.ContentType())
	return c.ShouldBindWith(obj, b)
}

//获取验证码
func GetVcode() (vcode string) {
again:
	rnd, err := crand.Int(crand.Reader, big.NewInt(10000))
	if err != nil {
		fmt.Printf("crand.Int() error : %v \\n", err)
		goto again
	}
	vcode = fmt.Sprintf("%04v", rnd)
	return
}

//LoginType
const (
	// LOGINAPP          = "login_app"
	// LOGINWECHAT       = "login_wechat"
	// LOGINWECHATNOTIFY = "login_small_wechat"
	// LOGINABC          = "login_abc"
	AUDIT_ACTION_LOGIN = "login"
	AUDIT_ACTION_SCAN  = "scan"
)

func GetAuditAction(action string, parameter string) string {
	return action + "_" + parameter
}

const base64Table = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/"

var coder = base64.NewEncoding(base64Table)

func Base64Encode(encode_byte []byte) []byte {
	return []byte(coder.EncodeToString(encode_byte))
}

func Base64Decode(decode_byte []byte) ([]byte, error) {
	return coder.DecodeString(string(decode_byte))
}

type ResultData struct {
	Data          interface{} `json:"data"`
	PageParameter Page        `json:"page_parameter"`
	InternelError string      `json:"internel_error"`
	ErrorMsg      string      `json:"error_msg"`
	Code          int         `json:"-"`
}
