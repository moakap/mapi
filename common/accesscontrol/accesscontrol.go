// Package accesscontrol provides a http handler middleware
// for allowing cross domain request.
package accesscontrol

import "net/http"

// Config for configuration of AllowCrossDomainRequest.
type Config struct {
	Origin string
}

// AllowCrossDomainRequest wraps an http.Handler to allow cross
// domain requests.
func AllowCrossDomainRequest(c Config) func(h http.Handler) http.Handler {
	return func(h http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, req *http.Request) {
			origin := c.Origin
			if origin == "" {
				origin = "*"
			}

			w.Header().Set("Access-Control-Allow-Origin", origin)
			w.Header().Set("Access-Control-Allow-Methods", "OPTIONS, GET, PATCH, PUT, POST, DELETE")
			w.Header().Set("Access-Control-Allow-Headers", "accept, authorization, content-type")

			if req.Method == "OPTIONS" {
				w.WriteHeader(http.StatusOK)
				return
			}
			h.ServeHTTP(w, req)
		})
	}
}
