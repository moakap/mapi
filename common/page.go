package common

import "github.com/jinzhu/gorm"

const DEFAULT_PAGESIZE = 10

type Page struct {
	TotalCount  int
	TotalPage   int
	CurrentPage int
	PageSize    int
}

type PageArray struct {
	Page
	Datas interface{} `json:"datas"`
}

func Pagination(db *gorm.DB, currentPage int, pageSize int) *gorm.DB {
	if pageSize == 0 {
		pageSize = DEFAULT_PAGESIZE
	}
	return db.Limit(pageSize).Offset(currentPage * pageSize)
}

func NewPageByArray(p int, datas []interface{}) PageArray {
	totalCount := len(datas)
	var last int
	if totalCount%DEFAULT_PAGESIZE != 0 {
		last = 1
	}
	total := totalCount/DEFAULT_PAGESIZE + last
	page := Page{
		TotalCount:  totalCount,
		TotalPage:   total,
		CurrentPage: p,
		PageSize:    DEFAULT_PAGESIZE,
	}
	pageArray := PageArray{
		Page:  page,
		Datas: datas[DEFAULT_PAGESIZE*p : DEFAULT_PAGESIZE*p+DEFAULT_PAGESIZE],
	}

	return pageArray
}

func NewPage(totalCount int, currentPage int, pageSize int) Page {
	if pageSize == 0 {
		pageSize = DEFAULT_PAGESIZE
	}
	var last int
	if totalCount%pageSize != 0 {
		last = 1
	}
	total := totalCount/pageSize + last
	return Page{
		TotalCount:  totalCount,
		TotalPage:   total,
		CurrentPage: currentPage,
		PageSize:    pageSize,
	}
}
